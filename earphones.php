<?php
session_start();

require_once "include/dbms.inc.php";
require_once "include/template2.inc.php";
require_once "include/utilities.inc.php";

if(isset($_GET['id'])){
    $id_pagina = $_GET['id'];
}
else{
    $id_pagina = cercaPaginadaDescrizione('earphones');
}
unset($_GET['id']);
##################
#dtml principale
$main = new Template('skin/dtml/t_frame-public.html');
##################

#carrello
$carrello = carrello();
if($carrello != NULL){
    $top_cart = new Template('skin/dtml/b_cart.html');
    $top_cart->setContent('price_amount', number_format($carrello[0],2));
    $top_cart->setContent('item_num', $carrello[1]);
    $top_cart->setContent('item_plural', $carrello[2]);
    $top_cart->setContent('id_cart', cercaPaginadaDescrizione('cart'));
    $main->setContent('cart', $top_cart->get());
}else{$top_cart = new Template('skin/dtml/b_cart.html');
    $top_cart->setContent('price_amount', '0.00');
    $top_cart->setContent('item_num', '0');
    $top_cart->setContent('item_plural', "");
    $top_cart->setContent('id_cart', cercaPaginadaDescrizione('cart'));
    $main->setContent('cart', $top_cart->get());
}

####################################################
#istanziazione degli oggetti necessari per la pagina
$nav_bar = new Template('skin/dtml/b_main_nav_bar.html');
$slider = new Template('skin/dtml/b_slider.html');
$search_bar = new Template('skin/dtml/b_search_bar.html');
$flt = new Template('skin/dtml/b_side_filters3.html');

#################################################
#query e impostazioni per la griglia dei prodotti
/*$griglia = new Template('skin/dtml/b_product_grid.html');

$query = "SELECT 5_product.* FROM 5_product,5_product_category,5_category 
    WHERE 5_category.descr = 'earphones' AND id_category = 5_category.id AND id_product=5_product.id 
    ORDER BY RAND();";
$ris = getResult($query);

$griglia->setContent('selettorePagine', "");
for($i = 0; $i < 9 && $i < count($ris);$i++){
    $prodotti[] = $ris[$i];
}
$griglia->setContent('prodotti', $prodotti);
$griglia->setContent('titolo','Earphones');
##################################################
*/
################################
#saluto utente oppure side login
if(isLogged()){
    $user_greetings=new Template('skin/dtml/b_side_user_greetings.html');
    $user_greetings->setContent('username', $_SESSION['username']);
    $main->setContent('user', $user_greetings->get() );

}
else{
    $side_login = new Template('skin/dtml/b_side_login.html');
    $side_login->setContent('id_page', $id_pagina);
    $main->setContent('login',$side_login->get());
}

###############################

#######
#slider
$ris = slides($id_pagina);
$slider->setContent('slides', $ris);
#######

##################################################
#sostituzione dei placehloder e close della pagina
$ris = menu('header');
$nav_bar->setContent('main_menu', $ris);
$search_bar->setContent('script', cercaPaginadaDescrizione('search'));
$nav_bar->setContent('search_bar', $search_bar->get());
$flt->setContent('cat', categorie());
$flt->setContent('rating', '');
$flt->setContent('priceRange', rangePrezzo());
$flt->setContent('discountRange', rangeSconto());
//$main->setContent('inner_container', $griglia->get());
$main->setContent('main_nav_bar',$nav_bar->get());
$main->setContent('slider',$slider->get());
$main->setContent('side_filters', $flt->get());$scripts = array();
$scripts[] = "product_grid.js";
$main->setContent('scripts',$scripts);
$main->close();
##################################################
?>
