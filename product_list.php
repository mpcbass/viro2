<?php

session_start();

require_once 'include/template2.inc.php';
require_once 'include/utilities.inc.php';


$id_pagina = $_GET['id'];
unset($_GET['id']);

function filtra($param){
    $n_cat = 0;
    $n_price = 0;
    $n_rating = 0;
    $n_discount = 0;
    $n_sens = 0;
    $n_imp = 0;
    $query = "SELECT * FROM 5_product WHERE ";
    $query_cat = "id IN (SELECT id_product FROM 5_product_category WHERE ";
    $query_price = "";
    $query_rating = "";
    $query_discount = "";
    $query_sens = "";
    $query_imp = "";
    $order_by = "";
    foreach($param as $k => $v){
        if(preg_match('/cat/',$k)){
            $n_cat++;
            if($n_cat == 1){
                $query_cat .= $v;
            }
            else{
                $query_cat .= " OR ".$v;
            }
        }
        if(preg_match('/price/',$k)){
            $n_price++;
            if($n_price > 1){
                $query_price .= " OR ";
            }
            else{
                $order_by .= " price";
            }
            $query_price .= $v;
        }
        if(preg_match('/rating/',$k)){
            $n_rating++;
            if($n_rating > 1){
                $query_rating .= " OR ";
            }
            else{
                $order_by .= " rating";
            }
            $query_rating .= $v;
        }
        if(preg_match('/discount/',$k)){
            $n_discount++;
            if($n_discount > 1){
                $query_discount .= " OR ";
            }else{
                $order_by .= " discount";
            }
            $query_discount .= $v;
        }
        if(preg_match('/impedence/',$k)){
            $n_imp++;
            if($n_imp > 1){
                $query_imp .= " OR ";
            }else{
                $order_by .= " discount";
            }
            $query_imp .= $v;
        }
        if(preg_match('/sensitivity/', $k)){
            $n_sens++;
            if($n_sens > 1){
                $query_sens .= " OR ";
            }else{
                $order_by .= " sensitivity";
            }
            $query_sens .= $v;
        }
    }
    if($n_cat > 1){
        $query_cat .= " GROUP BY(id_product) HAVING COUNT(*) = {$n_cat}";
    }
    if($n_cat == 0){
        $query_cat .= "1";
    }
    $query_cat .= ")";
    if($order_by != ""){
        $order_by = preg_replace('/(\ )/', "", $order_by, 1);
        $order_by = preg_replace('/\ /', ",", $order_by);
        $order_by = "ORDER BY ".$order_by;
        
    }
    if($query_price != ""){
        $query_price = "(".$query_price.") AND ";
    }
    if($query_discount != ""){
        $query_discount = "(".$query_discount.") AND ";
    }
    if($query_imp != ""){
        $query_imp = "(".$query_imp.") AND ";
    }
    if($query_sens != ""){
        $query_sens = "(".$query_sens.") AND ";
    }
    if($query_rating != ""){
        $query_rating = "(".$query_rating.") AND ";
    }
    $query .= $query_price.$query_discount.$query_imp.$query_sens.$query_rating.$query_cat.$order_by.";";
    return getResult($query);
}

################
#dtml principale
$main = new Template('skin/dtml/t_frame-public.html');
###############

################
#menu principale
$nav_bar = new Template('skin/dtml/b_main_nav_bar.html');
$ris = menu('header');
$nav_bar->setContent('main_menu', $ris);

$search_bar = new Template('skin/dtml/b_search_bar.html');
$search_bar->setContent('script', cercaPaginadaDescrizione('search'));
$nav_bar->setContent('search_bar', $search_bar->get());
################

################################
#saluto utente oppure side login
if(isLogged()){
    $user_greetings=new Template('skin/dtml/b_side_user_greetings.html');
    $user_greetings->setContent('username', $_SESSION['username']);
    $main->setContent('user', $user_greetings->get() );

}
else{
    
    $side_login = new Template('skin/dtml/b_side_login.html');
    $side_login->setContent('id_page', $id_pagina);
    $main->setContent('login',$side_login->get());
}

###############################

#########
#carrello
$carrello = carrello();
if($carrello != NULL){
    $top_cart = new Template('skin/dtml/b_cart.html');
    $top_cart->setContent('price_amount', number_format($carrello[0],2));
    $top_cart->setContent('item_num', $carrello[1]);
    $top_cart->setContent('item_plural', $carrello[2]);
    $top_cart->setContent('id_cart', cercaPaginadaDescrizione('cart'));
    $main->setContent('cart', $top_cart->get());
}else{$top_cart = new Template('skin/dtml/b_cart.html');
    $top_cart->setContent('price_amount', '0.00');
    $top_cart->setContent('item_num', '0');
    $top_cart->setContent('item_plural', "");
    $top_cart->setContent('id_cart', cercaPaginadaDescrizione('cart'));
    $main->setContent('cart', $top_cart->get());
}
#########

$side_filter = new Template('skin/dtml/b_side_filters3.html');

$side_filter->setContent('rating', "");
$side_filter->setContent('priceRange', rangePrezzo());
$side_filter->setContent('discountRange', rangeSconto());

$side_filter->setContent('cat', categorie());

/*$delete = array();
$set = array();
$da = 0;
#############################################
#controllo con rimozione e aggiunta di filtri
if(isset($_GET)){
    foreach($_GET as $k => $v){
        if($k == 'el'){
            $da = $v;
        }
    }
    #################
    #griglia prodotti
    $griglia = new Template('skin/dtml/b_product_grid_search.html');
    $res = filtra($_GET);
    $info_page_selector['num_righe'] = count($res); 
    $info_page_selector['da'] = $da;
    $griglia->setContent('selettorePagine', $info_page_selector);
    $elementi_in_pagina = array();
    for($i = $da; $i < count($res);$i++){
        $elementi_in_pagina[] = $res[$i];
    }
    if($res == 0){
        $elementi_in_pagina = 0;
    }
    $griglia->setContent('prodotti', $elementi_in_pagina);
    $griglia->setContent('titolo','products');
    #################
}
#############################################
*/

$main->setContent('main_nav_bar',$nav_bar->get());
//$main->setContent('inner_container', );
$main->setContent("side_filters", $side_filter->get());
$scripts = array();
$scripts[] = "product_grid.js";
$main->setContent('scripts',$scripts);
$main->close();
?>
