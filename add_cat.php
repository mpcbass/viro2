<?php

require_once 'include/utilities.inc.php';
require_once 'include/template2.inc.php';

if (isset($_GET['add'])) {
    $_GET = avoid_sql_injection($_GET);
    $query = "INSERT INTO 5_category VALUES('',{$_GET['sub_cat']},'{$_GET['name']}');";
    if (!queryInsert($query)) {
        
    } else {
        $query = "INSERT INTO 1_service VALUES('','" . $_GET['name'] . ".php');";
        if (!queryInsert($query)) {
            
        } else {
            $ris = getResult("SELECT id FROM 1_group WHERE descr ='p_user' OR descr ='t_user';");
            $query = "INSERT INTO 1_service_group VALUES(" . cercaPaginadaDescrizione($_GET['name']) . "," . $ris[0]['id'] . ");";
            queryInsert($query);
            $query = "INSERT INTO 1_service_group VALUES(" . cercaPaginadaDescrizione($_GET['name']) . "," . $ris[1]['id'] . ");";
            queryInsert($query);
            if ($_GET['sub_cat'] == 0) {
                $rs = getResult("SELECT id FROM 2_menu_field WHERE value LIKE 'product';");
                $id_parent = $rs[0]['id'];
                $query = "SELECT MAX(posizione) AS 'pos' FROM 2_menu_field WHERE id_parent = {$id_parent};";
            } else {
                $query = "SELECT descr FROM 5_category WHERE id={$_GET['sub_cat']}";
                $cat_name = getResult($query);
                $rs = getResult("SELECT id FROM 2_menu_field WHERE value LIKE '{$cat_name['0']['descr']}'");
                $id_parent = $rs[0]['id'];
                $query = "SELECT MAX(posizione)AS 'pos' FROM 2_menu_field WHERE id_parent = {$id_parent};";
            }
            $parent_menu = getResult($query);
            $query = "INSERT INTO 2_menu_field 
                VALUES('','{$_GET['name']}',{$id_parent}," . ($parent_menu[0]['pos'] + 1) . ",'index.php?id=" . cercaPaginadaDescrizione($_GET['name']) . "');";
            queryInsert($query);
            $query = "SELECT id FROM 2_menu_field WHERE value = '{$_GET['name']}';";
            $rs = getResult($query);
            $query = "SELECT id FROM 2_menu WHERE descr = 'header';";
            $rs1 = getResult($query);
            $query = "INSERT INTO 2_menu_composition VALUES({$rs1[0]['id']},{$rs[0]['id']},0);";
            queryInsert($query);
        }
    }
    header('location:index.php?mex=product category added correctly!');
} else if (isset($_GET['del'])) {
    $query = "DELETE FROM 5_category WHERE id =" . $_GET['cat'] . ";";
    queryInsert($query);
    header("location:index.php?mex=product category correctly deleted!");
} else {
    $main = new Template('skin/dtml/t_frame_private.html');
    $nav_bar = new Template('skin/dtml/b_main_nav_bar.html');
    $side_menu = new Template('skin/dtml/b_side_menus.html');
    $form = new Template('skin/dtml/b_form_add_cat.html');
    $query = "SELECT * FROM 5_category WHERE 1;";
    $ris = getResult($query);
    $form->setContent('cat_list', $ris);
    $form->setContent('cat_list2', $ris);
    $this_page = cercaPaginadaDescrizione('add_cat');
    $form->setContent('id_page', $this_page);
    $form->setContent('id_page1', $this_page);
    $ris = menu('header_admin');
    $nav_bar->setContent('main_menu', $ris);

    $side_menu->setContent('page_cat', $this_page);
    $side_menu->setContent('page_slide', cercaPaginadaDescrizione('slides'));
    $side_menu->setContent('page_order', cercaPaginadaDescrizione('manage_order'));
    $side_menu->setContent('page_adprod', cercaPaginadaDescrizione('manage_prod'));
    $side_menu->setContent('page_user', cercaPaginadaDescrizione('manage_user'));
    $main->setContent('inner_container', $form->get());
    $main->setContent('side_menu', $side_menu->get());
    $main->setContent('main_nav_bar', $nav_bar->get());
    $main->close();
}
?>
