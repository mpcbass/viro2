<?php

session_start();
require_once 'include/dbms.inc.php';
require_once 'include/utilities.inc.php';
require_once 'include/template2.inc.php';

$this_page = cercaPaginadaDescrizione('modify_product');
$search = 0;

foreach($_GET as $k => $v){
    if(preg_match('/search/',$k)){
        $search = 1;
    }
}
if ($search == 1) {
    $main = new Template('skin/dtml/t_frame_private.html');
    $side_menu = new Template('skin/dtml/b_side_menus.html');
    $nav_bar = new Template('skin/dtml/b_main_nav_bar.html');
    $form = new Template('skin/dtml/b_mods_product.html');

    $form->setContent('id_page', $this_page);
    if(isset($_GET['searchID'])){
        $query = "SELECT * FROM 5_product WHERE id = {$_GET['ID']};";
    }  else {
        $query = "SELECT * FROM 5_product WHERE name = '{$_GET['name']}'";
    }
    $ris = getResult($query);
    if ($ris <= 0) {
        header("location:index.php?id=".$this_page."&err=a product with specified parameters does not exists!");
    } else {
        foreach ($ris[0] as $k => $v) {
            $form->setContent($k, $v);
        }
        $form->setContent('id_prod', $ris[0]['id']);
        $query = "SELECT descr FROM 5_category,5_product_category WHERE id_product = {$ris[0]['id']} AND 5_category.id = 5_product_category.id_category;";
        $ris = getResult($query);
        $cats = "";
        foreach ($ris as $k => $v){
            $cats .= $v['descr']." ";
        }
        $form->setContent('cat',$cats);
    }
    if (isset($_GET['err'])) {
        $form->setContent('error_message', $_GET['err']);
    }
    $ris = menu('header_admin');
    $nav_bar->setContent('main_menu', $ris);

    $side_menu->setContent('page_slide', cercaPaginadaDescrizione('slides'));
    $side_menu->setContent('page_order', cercaPaginadaDescrizione('manage_order'));
    $side_menu->setContent('page_cat', cercaPaginadaDescrizione('add_cat'));
    $side_menu->setContent('page_adprod', cercaPaginadaDescrizione('manage_prod'));
    $side_menu->setContent('page_mprod', $this_page);
    $side_menu->setContent('page_user', cercaPaginadaDescrizione('manage_user'));
    $side_menu->setContent('page_muser', cercaPaginadaDescrizione('modify_user'));

    $main->setContent('side_menu', $side_menu->get());
    $main->setContent('main_nav_bar', $nav_bar->get());
    $main->setContent('inner_container', $form->get());
    $main->close();
} else if (isset($_GET['mods'])) {
    $query = "UPDATE 5_product SET WHERE id = {$_GET['ID']};";
    if (queryInsert($query)) {
        header("location:index.php?mex=user updated successfully!");
    } else {
        header("location:index.php?id=" . $this_page . "&err=parameters not valid!");
    }
} else {
    $main = new Template('skin/dtml/t_frame_private.html');
    $side_menu = new Template('skin/dtml/b_side_menus.html');
    $nav_bar = new Template('skin/dtml/b_main_nav_bar.html');
    $form = new Template('skin/dtml/b_search_product.html');

    $form->setContent('id_page1', $this_page);
    $form->setContent('id_page2', $this_page);
    if (isset($_GET['err'])) {
        $form->setContent('error_message', $_GET['err']);
    }
    $ris = menu('header_admin');
    $nav_bar->setContent('main_menu', $ris);

    $side_menu->setContent('page_slide', cercaPaginadaDescrizione('slides'));
    $side_menu->setContent('page_order', cercaPaginadaDescrizione('manage_order'));
    $side_menu->setContent('page_cat', cercaPaginadaDescrizione('add_cat'));
    $side_menu->setContent('page_adprod', cercaPaginadaDescrizione('manage_product'));
    $side_menu->setContent('page_user', cercaPaginadaDescrizione('manage_user'));
    $side_menu->setContent('page_muser', cercaPaginadaDescrizione('modify_user'));
    $side_menu->setContent('page_mprod', $this_page);
    
    $main->setContent('side_menu', $side_menu->get());
    $main->setContent('main_nav_bar', $nav_bar->get());
    $main->setContent('inner_container', $form->get());
    $main->close();
}
?>
