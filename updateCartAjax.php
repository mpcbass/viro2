<?php
session_start();

require_once 'include/dbms.inc.php';
require_once 'include/utilities.inc.php';

function delete_carrello(){
    $query = "SELECT id FROM 1_user WHERE username = '{$_SESSION['username']}';";
    $id_user = getResult($query);
    $query = "DELETE FROM 5_cart WHERE id_user = {$id_user[0]['id']};";
    queryInsert($query);
}


$data = json_decode($_POST['data']);
if($_POST['deleteCart']=='false'){
    unset($_SESSION['cart']['prods']);
    foreach($data as $k => $v){
        $_SESSION['cart']['prods'][$v->{'id'}] = $v->{'qty'};
    }
    $_SESSION['cart']['tot'] = $_POST['tot'];
    $shipping_cost = ($_POST['tot']>20)?0:5;
    salvaCarrello();
} else {
    unset($_SESSION['cart']['prods']);
    unset($_SESSION['cart']);
    if(isLogged()){
        delete_carrello();
    }
    $shipping_cost = 5;
}

$obj= (object) array('shipping'=>$shipping_cost);

echo json_encode($obj);
/*
$prods = "";
for ($i = 0; $i < count($data); $i++) {
    $prods.="|" ;
    for ($j = 0; $j < count($data[$i]); $j++)  {
        $prods.=$data[$i][$j].($j==0?"=":"");
    }
}
echo $prods;
fore*/
?>
