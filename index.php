<?php

session_start();

#echo "</br>".  session_id() ."</br>";

require_once 'include/utilities.inc.php';
require_once 'include/dbms.inc.php';
require_once 'include/accessMng.inc.php';

/*
 *  si itanzia un oggetto della classe access manager 
 *  che si occupa di verificare la validità della sessione
 */
$auth = new accessMng();

/*
 *  si esegue il controllo della get per il paramentro id_pagina
 *  a questo punto, se il parametro è vuoto si indirizza alla home(id=1)
 *  se no si indirizza alla pagina con id nella get e poi si verifica se l'utente 
 *  può accedervi
 */
if(isset($_GET['id']) && $_GET['id'] != NULL){
    $page_data=$_GET['id'];
}
else{
    if(isAdmin()){
        $page_data = cercaPaginadaDescrizione('home_admin');
    }
    else{
        $page_data = cercaPaginadaDescrizione('home');
    }
}
 
$permission = $auth->accessPage($page_data);
if($permission)
{
   require $_SESSION['page'][$page_data];
}
else
{
    #errore di autorizzazione
    header("location:error.php?e_type=auth");
}
?>
