<?php

session_start();
require_once 'include/template2.inc.php';
require_once 'include/dbms.inc.php';
require_once 'include/utilities.inc.php';

$this_page = $_GET['id'];

if (isset($_GET['del_order'])) {
    if (isset($_GET['n_ord'])) {
        $n_ord = $_GET['n_ord'];
    } else {
        $n_ord = 0;
    }
    $query = "SELECT id_prod FROM 7_order_detail WHERE id_ord = {$n_ord};";
    $products = getResult($query);
    $query = "SELECT state FROM 7_order WHERE n_ord = {$n_ord};";
    $state = getResult($query);
    $query = "DELETE FROM 7_order WHERE n_ord = {$n_ord};";
    queryInsert($query);
    if ($state[0]['state'] == 'delivering') {
        foreach ($products as $k => $v) {
            $query = "UPDATE 5_product SET qty = qty + {$v['qty']} WHERE id = {$v['id_prod']};";
            queryInsert($query);
        }
    }
    header("location:index.php?mex=order deleted correctly!");
} else if (isset($_GET['add_order'])) {
    $_GET['date'] = preg_replace("/([\w]+)([\/\-])([\w]+)([\/\-])([\w]+)/", "\$1,\$3,\$5", $_GET['date']);
    $query = "INSERT INTO 7_order VALUES('',STR_TO_DATE('{$_GET['date']}','%d,%m,%Y'),{$_GET['id_user']},{$_GET['shipping_costs']},{$_GET['VAT']},{$_GET['total']},
        '{$_GET['address']}','{$_GET['city']}','{$_GET['country']}','{$_GET['ZIP']}','{$_GET['dest_name']}','{$_GET['dest_surname']}','{$_GET['shipping_address']}','{$_GET['shipping_country']}','{$_GET['shipping_city']}','{$_GET['shipping_ZIP']}','{$_GET['cart_number']}','{$_GET['state']}');";
    if (queryInsert($query)) {
        $n_ord = getResult("SELECT MAX(n_ord) AS 'n_ord' FROM 7_order WHERE 1;");
        header("location:index.php?mex=new order number: " . $n_ord[0]['n_ord']);
    } else {
        header("location:index.php?id=" . $this_page . "&neword_err=parameters not valid!");
    }
} else if (isset($_GET['add_detail'])) {
    $query = "INSERT INTO 7_order_detail VALUES({$_GET['n_ord']},{$_GET['id_prod']},{$_GET['qty']},{$_GET['total']});";
    if (queryInsert($query)) {
        header("location:index.php?mex=detail added at order {$_GET['n_ord']}!");
    } else {
        header("location:index.php?id={$this_page}&newdetail_err=parameters not valid!");
    }
} else {
    $main = new Template('skin/dtml/t_frame_private.html');
    $nav_bar = new Template('skin/dtml/b_main_nav_bar.html');
    $side_menu = new Template('skin/dtml/b_side_menus.html');
    $form = new Template('skin/dtml/b_manage_order.html');
    $query = "SELECT * FROM 7_delivery_cost;";
    $ris = getResult($query);
    $form->setContent('delivery_costs', $ris);
    $form->setContent('id_page', $this_page);
    $form->setContent('id_page1', $this_page);
    $form->setContent('id_page2', $this_page);
    $query = "SELECT id,name FROM 5_product WHERE 1;";
    $ris = getResult($query);
    $form->setContent('product_list', $ris);
    if (isset($_GET['newdetail_err'])) {
        $form->setContent('detail_message', $_GET['newdetail_err']);
    }
    if (isset($_GET['neword_err'])) {
        $form->setContent('order_message', $_GET['neword_err']);
    }
    $ris = menu('header_admin');
    $nav_bar->setContent('main_menu', $ris);
    $side_menu->setContent('page_cat', cercaPaginadaDescrizione('add_cat'));
    $side_menu->setContent('page_slide', cercaPaginadaDescrizione('slides'));
    $side_menu->setContent('page_order', $this_page);
    $side_menu->setContent('page_user', cercaPaginadaDescrizione('manage_user'));
    $side_menu->setContent('page_muser', cercaPaginadaDescrizione('modify_user'));
    $side_menu->setContent('page_mprod', cercaPaginadaDescrizione('modify_product'));
    $side_menu->setContent('page_adprod', cercaPaginadaDescrizione('manage_prod'));
    $main->setContent('inner_container', $form->get());
    $main->setContent('side_menu', $side_menu->get());
    $main->setContent('main_nav_bar', $nav_bar->get());
    $main->close();
}
?>
