<?php

session_start();
require_once 'include/dbms.inc.php';
require_once 'include/template2.inc.php';
require_once 'include/utilities.inc.php';

if (isset($_GET['id'])) {
    $id_pagina = $_GET['id'];
} else {
    $id_pagina = cercaPaginadaDescrizione('product');
}
unset($_GET['id']);
$utente = array();

$carrello = carrello();
##################################################################
#aggiungi prodotti nel carrello inseriti dalla pagina del prodotto
if (isset($_GET['insert']) && isset($_GET['id_prod'])) {
    if (!isset($_SESSION['cart']['tot'])) {
        $_SESSION['cart']['tot'] = number_format(0.00, 2, '.', '');
    }
    #se il prodotto era già nel carrello allora si deve rimpiazzare (non aggiungere) con la nuova quantità
    if (isset($_SESSION['cart']['prods'][$_GET['id_prod']])) {
        $_SESSION['cart']['tot'] = number_format(round($_SESSION['cart']['tot'] - $_GET['prezzo'] * $_SESSION['cart']['prods'][$_GET['id_prod']], 2), 2, '.', '');
    }
    $_SESSION['cart']['tot'] = number_format(round($_SESSION['cart']['tot'] + $_GET['prezzo'] * $_GET['num'], 2), 2, '.', '');
    $_SESSION['cart']['prods'][$_GET['id_prod']] = $_GET['num'];
    salvaCarrello();
    header("location: index.php?id=" . cercaPaginadaDescrizione('product') . "&id_prod={$_GET['id_prod']}&addcart=");
}
##################################################################
##################
#aggiorna carrello
if (isset($_GET['update_cart'])) {
    foreach ($_GET as $k => $v) {
        if (preg_match('/(qta_prod)(\d+)/', $k, $matches)) {
            $query = "SELECT price FROM 5_product WHERE id={$matches[2]};";
            $price = getResult($query);
            $_SESSION['cart']['tot'] += round($v * $price[0]['price'], 2) -
                    round($_SESSION['cart']['prods'][$matches[2]] * $price[0]['price'], 2);
            $_SESSION['cart']['prods'][$matches[2]] = round($v, 2);
        }
    }
    salvaCarrello();
    header("location:index.php?id={$id_pagina}");
}
##################
#################
#rimuovi prodotto
if (isset($_GET['remove'])) {
    $_SESSION['cart']['tot'] -= $_GET['prezzo'] * $_SESSION['cart']['prods'][$_GET['remove']];
    unset($_SESSION['cart']['prods'][$_GET['remove']]);
    salvaCarrello();
    if (count($_SESSION['cart']['prods']) < 1) {
        unset($_SESSION['cart']);
        $query = "DELETE FROM 5_cart WHERE id_user = {$utente[0]['id']};";
        queryInsert($query);
    }
}
#################
####################
#proceed to checkout
if (isset($_GET['proceed'])) {
    $parametri_form = "";
    $query = "SELECT * FROM 1_user WHERE username = '{$_SESSION['username']}';";
    $user = getResult($query);
    $query = "SELECT * FROM 7_order WHERE id_user = {$user[0]['id']} ORDER BY date DESC LIMIT 1;";
    $ris = getResult($query);
    if ($ris != 0) {
        $parametri_form = "&address={$ris[0]['address']}&country={$ris[0]['country']}&city={$ris[0]['city']}&zip={$ris[0]['ZIP']}";
        $parametri_form .= "&name={$user[0]['Name']}&surname={$user[0]['Surname']}&phone={$user[0]['phone']}&email={$user[0]['email']}";
    }
    header("location:index.php?id=" . cercaPaginadaDescrizione('checkout') . $parametri_form);
}
####################
##################
#dtml principale
$main = new Template('skin/dtml/t_frame-public.html');
##################
####################################################
#istanziazione degli oggetti necessari per la pagina
$nav_bar = new Template('skin/dtml/b_main_nav_bar.html');
$search_bar = new Template('skin/dtml/b_search_bar.html');
$flt = new Template('skin/dtml/b_side_filters3.html');
$cart = new Template('skin/dtml/b_cart_list.html');

################################
#saluto utente oppure side login
if (isLogged()) {
    $user_greetings = new Template('skin/dtml/b_side_user_greetings.html');
    $user_greetings->setContent('username', $_SESSION['username']);
    $main->setContent('user', $user_greetings->get());
} else {
    $side_login = new Template('skin/dtml/b_side_login.html');
    $side_login->setContent('id_page', $id_pagina);
    $main->setContent('login', $side_login->get());
}

###############################
#########################
#visualizzazione carrello
$cart->setContent('id_page', cercaPaginadaDescrizione('cart'));
if ($carrello == NULL) {
    $cart->setContent('tabella', "");
} else {
    $ids_prod = array();
    $q_prod = array();
    foreach ($_SESSION['cart']['prods'] as $k => $v) {
        if (!isset($v)) {
            continue;
        }
        $ids_prod[] = "id=" . $k;
        $q_prod[$k] = $v;
    }
    $query_prod = implode(' OR ', $ids_prod);
    $query = "SELECT * FROM 5_product WHERE " . $query_prod . ";";
    $ris = getResult($query);
    foreach ($ris as $k => $v) {
        $ris[$k]['qty_ord'] = $q_prod[$ris[$k]['id']];
    }
    $cart->setContent('tabella', $ris);
}
#########################
##################################################
#sostituzione dei placehloder e close della pagina
$ris = menu('header');
$nav_bar->setContent('main_menu', $ris);
$search_bar->setContent('script', cercaPaginadaDescrizione('search'));
$nav_bar->setContent('search_bar', $search_bar->get());
$flt->setContent('cat', categorie());
$flt->setContent('rating', '');
$flt->setContent('priceRange', rangePrezzo());
$flt->setContent('discountRange', rangeSconto());
$main->setContent('inner_container', $cart->get());
$main->setContent('main_nav_bar', $nav_bar->get());
$main->setContent('side_filters', $flt->get());
$scripts = array();
$scripts[] = "cart_ajax.js";
$main->setContent('scripts', $scripts);
$main->close();
##################################################
?>
