<?php
require_once 'include/utilities.inc.php';
session_start();
/*EVENTUALE DUMP DI TUTTE LE INFORMAZIONI*/
if(isLogged()){
    session_unset();
    session_destroy();
}
header('location: index.php');
?>
