<?php

session_start();

require_once "include/dbms.inc.php";
require_once "include/template2.inc.php";
require_once "include/utilities.inc.php";

if (isset($_GET['id'])) {
    $id_pagina = $_GET['id'];
} else {
    $id_pagina = cercaPaginadaDescrizione('checkout');
    $_GET['id'] = $id_pagina;
}
//unset($_GET['id']);
##################
#dtml principale
$main = new Template('skin/dtml/t_frame-public.html');
##################
#carrello
$carrello = carrello();
if ($carrello != NULL) {
    $top_cart = new Template('skin/dtml/b_cart.html');
    $top_cart->setContent('price_amount', number_format($carrello[0], 2));
    $top_cart->setContent('item_num', $carrello[1]);
    $top_cart->setContent('item_plural', $carrello[2]);
    $top_cart->setContent('id_cart', cercaPaginadaDescrizione('cart'));
    $main->setContent('cart', $top_cart->get());
} else {
    $top_cart = new Template('skin/dtml/b_cart.html');
    $top_cart->setContent('price_amount', '0.00');
    $top_cart->setContent('item_num', '0');
    $top_cart->setContent('item_plural', "");
    $top_cart->setContent('id_cart', cercaPaginadaDescrizione('cart'));
    $main->setContent('cart', $top_cart->get());
}
####################################################
#istanziazione degli oggetti necessari per la pagina
$nav_bar = new Template('skin/dtml/b_main_nav_bar.html');
$search_bar = new Template('skin/dtml/b_search_bar.html');
$flt = new Template('skin/dtml/b_side_filters3.html');
$form = new Template('skin/dtml/b_checkout.html');

################################
#saluto utente oppure side login
if (isLogged()) {
    $user_greetings = new Template('skin/dtml/b_side_user_greetings.html');
    $user_greetings->setContent('username', $_SESSION['username']);
    $main->setContent('user', $user_greetings->get());
} else {
    $side_login = new Template('skin/dtml/b_side_login.html');
    $side_login->setContent('id_page', $id_pagina);
    $main->setContent('login', $side_login->get());
}

##############################

if (isset($_POST['place_order'])) {
    $query = "SELECT id FROM 1_user WHERE username = '{$_SESSION['username']}';";
    $id = getResult($query);
    if ($_POST['payment_method'] == "bacs") {
        $first_name_card = $_POST['cred_card_first_name'];
        $last_name_card = $_POST['cred_card_last_name'];
        $cvv = $_POST['cred_card_cvv'];
        $n_card = $_POST['cred_card_num'];
    } else {
        $first_name_card = "";
        $last_name_card = "";
        $cvv = "";
        $n_card = "";
    }
    $query = "INSERT INTO 7_order VALUES('',CURRENT_DATE(),{$id[0]['id']},{$_POST['shipping_cost']},'21',{$_POST['total']},'{$_POST['billing_address_1']}','{$_POST['billing_city']}','{$_POST['billing_country']}','{$_POST['billing_postcode']}','{$_POST['shipping_first_name']}','{$_POST['shipping_last_name']}','{$_POST['shipping_address_1']}','{$_POST['shipping_country']}','{$_POST['shipping_city']}','{$_POST['shipping_postcode']}','{$first_name_card}','{$last_name_card}','{$cvv}','{$n_card}','delivering','{$_POST['order_comments']}');";
    queryInsert($query);
    $n_ord = getResult("SELECT MAX(n_ord) AS 'n_ord' FROM 7_order WHERE 1;");
    foreach ($_SESSION['cart']['prods'] as $k => $v) {
        $price = getResult("SELECT price FROM 5_product WHERE id={$k}");
        $tot = $price[0]['price'] * $v;
        $query = "INSERT INTO 7_order_detail VALUES({$n_ord[0]['n_ord']},{$k},{$v},{$tot});"; #TRIGGER QUANTITÀ
        queryInsert($query);
    }
    $query = "DELETE FROM 5_cart WHERE id_user = {$id[0]['id']}";
    queryInsert($query);
} else {
##################################################
#sostituzione dei placehloder e close della pagina
    $ris = menu('header');
    $nav_bar->setContent('main_menu', $ris);
    $search_bar->setContent('script', cercaPaginadaDescrizione('search'));
    $nav_bar->setContent('search_bar', $search_bar->get());
    $flt->setContent('cat', categorie());
    $flt->setContent('rating', '');
    $flt->setContent('priceRange', rangePrezzo());
    $flt->setContent('discountRange', rangeSconto());

    foreach ($_SESSION['cart']['prods'] as $k => $v) {
        if (!isset($v)) {
            continue;
        }
        $ids_prod[] = "id=" . $k;
        $q_prod[$k] = $v;
    }
    $query_prod = implode(' OR ', $ids_prod);
    $query = "SELECT * FROM 5_product WHERE " . $query_prod . ";";
    $ris = getResult($query);
    foreach ($ris as $k => $v) {
        $ris[$k]['qty_ord'] = $q_prod[$ris[$k]['id']];
    }
    $form->setContent('dettaglio', $ris);
    $form->setContent('netto', $_SESSION['cart']['tot']);
    $form->setContent('total', $_SESSION['cart']['tot']);
    $form->setContent('t', $_SESSION['cart']['tot']);
    $form->setContent('s', "0.00");
    $main->setContent('inner_container', $form->get());
    $main->setContent('main_nav_bar', $nav_bar->get());
    $main->setContent('side_filters', $flt->get());
    $scripts = array();
    $scripts[] = "checkout-form.js";
    $main->setContent('scripts', $scripts);
    $main->close();
}
?>
