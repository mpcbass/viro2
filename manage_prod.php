<?php

session_start();

require_once 'include/template2.inc.php';
require_once 'include/dbms.inc.php';
require_once 'include/utilities.inc.php';

$valori = "";
#funzione che restituisce 0 se non è stato settato nessun campo, 1 se tutti i 
#+campi sono corretti e una stringa contenente i nomi dei campi errati nel caso ce ne siano

function verifica_campi() {
    global $valori;
    $campi_errati = "";

    $_POST = avoid_sql_injection($_POST);
    if ($_POST['name'] != NULL) {
        $valori = "'{$_POST['name']}',";
    } else {
        $campi_errati .="%20name";
    }
    if ($_POST['brand'] != NULL) {
        $valori .= "'{$_POST['brand']}',";
    } else {
        $campi_errati .="%20brand";
    }
    if ($_POST['b_descr'] != NULL) {
        $valori .= "'" . htmlspecialchars($_POST['b_descr']) . "',";
    } else {
        $campi_errati .="%20brief_description";
    }
    if ($_POST['l_descr'] != NULL) {
        $valori .= "'" . htmlspecialchars($_POST['l_descr']) . "',";
    } else {
        $campi_errati .="%20long_description";
    }
    if ($_POST['features'] != NULL) {
        if (preg_match('/.\|/', $_POST['features'])) {
            $valori .= "'" . htmlspecialchars($_POST['features']) . "',";
        } else {
            $campi_errati .= "%20features";
        }
    } else {
        $campi_errati .= "%20features";
    }
    if ($_POST['impedence'] != NULL) {
        if (preg_match('/[a-z\_\-\+A-Z]+/', $_POST['impedence'])) {
            $campi_errati .= "%20impedence";
        }
        $valori .= "{$_POST['impedence']},";
    } else {
        $campi_errati .= "%20impedence";
    }
    if ($_POST['sensitivity'] != NULL) {
        if (preg_match('/[a-z\_\-\+A-Z]+/', $_POST['sensitivity'])) {
            $campi_errati .= "%20sensitivity";
        }
        $valori .= "{$_POST['sensitivity']},";
    } else {
        $campi_errati .= "%20sensitivity";
    }
    if ($_POST['price'] != NULL) {
        $valori .= "{$_POST['price']},";
        if (!preg_match('/[0-9]+\.[0-9][0-9]/', $_POST['price'])) {
            $campi_errati .= "%20price";
        }
    } else {
        $campi_errati .= "%20price";
    }
    if ($_POST['qty'] != NULL) {
        if (preg_match('/[a-z\_\-\+A-Z]+/', $_POST['qty'])) {
            $campi_errati .= "%20quantity";
        }
        $valori .= "{$_POST['qty']},";
    } else {
        $campi_errati .= "%20quantity";
    }
    if ($_POST['discount'] != NULL) {
        if (preg_match('/[a-z\_\-\+A-Z]+/', $_POST['discount'])) {
            $campi_errati .= "%20discount";
        }
        $valori .= "{$_POST['discount']},";
    }
    if ($_FILES['medium_img'] != NULL) {
        $valori .= "'skin/images/placeholder/{$_FILES['medium_img']['name']}',";
    } else {
        $campi_errati .="%20medium_image";
    }
    if ($_FILES['big_img'] != NULL) {
        $valori .= "'skin/images/placeholder/{$_FILES['big_img']['name']}',";
    } else {
        $campi_errati .="%20big_image";
    }
    if ($_FILES['small_img'] != NULL) {
        $valori .= "'skin/images/placeholder/{$_FILES['small_img']['name']}',";
    } else {
        $campi_errati .="%20small_image";
    }
    if ($_POST['alt_text'] != NULL) {
        $valori .= "'{$_POST['alt_text']}'";
    } else {
        $campi_errati .="%20alternative_text";
    }
    #valore di ritorno
    if ($campi_errati != "") {
        return $campi_errati;
    }
    return 1;
}

if (isset($_REQUEST['del'])) {
    $query = "DELETE FROM 5_product WHERE name = '{$_REQUEST['name']}';";
    if(queryInsert($query)){
        header('location:index.php');
    } else {
        header("location:index.php?mex=the product {$_REQUEST['name']} does not exist!");
    }
} else if (isset($_POST['add'])) {
    $cat = $_POST['cat'];
    $ris = verifica_campi();
    if ($ris === 1) {
        $query = "INSERT INTO 5_product VALUES(''," . $valori . ",0,0);";
        if (queryInsert($query)) {
            $query = "SELECT id FROM 5_product WHERE name = '{$_POST['name']}'";
            $id_prod = getResult($query);
            foreach ($cat as $v) {
                $query = "INSERT INTO 5_product_category VALUES({$id_prod[0]['id']},{$v});";
                queryInsert($query);
            }
            $mex = "'{$_POST['name']}' added to products!<br/>";
            header("location:index.php?mex=" . $mex);
        } else {
            header("location:error.php?e_type=add_prod_fail%20tutti");
        }
    } else {
        $campi_compilati = "";
        foreach ($_POST as $k => $v) {
            $campi_compilati .= "&{$k}={$v}";
        }
        header("location:error.php?e_type=add_prod_fail%20" . $ris . $campi_compilati);
    }
} else {
    $this_page = cercaPaginadaDescrizione('manage_prod');
    $main = new Template('skin/dtml/t_frame_private.html');
    $side_menu = new Template('skin/dtml/b_side_menus.html');
    $nav_bar = new Template('skin/dtml/b_main_nav_bar.html');
    $form = new Template('skin/dtml/b_manage_product.html');

    $form->setContent('id_page', $this_page);
    $query = "SELECT * FROM 5_category WHERE 1;";
    $ris = getResult($query);
    $form->setContent('cat_list', $ris);

    if (isset($_GET['err'])) {
        $campi = explode("%20", $_GET['err']);
        $string = "recheck the following fields: ";
        foreach ($campi as $k => $v) {
            $string.="$v <br/>";
        }
        $form->setContent('error_message', "<p>" . $string . "</p>");
        unset($_GET['err']);
        #
        foreach ($_GET as $k => $v) {
            $form->setContent($k, $v);
        }
    }

    $ris = menu('header_admin');
    $nav_bar->setContent('main_menu', $ris);

    $side_menu->setContent('page_slide', cercaPaginadaDescrizione('slides'));
    $side_menu->setContent('page_order', cercaPaginadaDescrizione('manage_order'));
    $side_menu->setContent('page_cat', cercaPaginadaDescrizione('add_cat'));
    $side_menu->setContent('page_adprod', $this_page);
    $side_menu->setContent('page_user', cercaPaginadaDescrizione('manage_user'));
    $side_menu->setContent('page_muser', cercaPaginadaDescrizione('modify_user'));
    $side_menu->setContent('page_mprod', cercaPaginadaDescrizione('modify_product'));
    
    $main->setContent('side_menu', $side_menu->get());
    $main->setContent('main_nav_bar', $nav_bar->get());
    $main->setContent('inner_container', $form->get());
    $main->close();
}
?>
