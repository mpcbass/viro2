$(document).ready(function() {

    function showTooltip(baseEl, msg, margin, timeout, storeTooltip) {
        var alreadyHasTooltip=false;
        $(baseEl).parents('section').find(".tempTooltip").each(function(i,el){
            if(el.attributes.name.nodeValue === $(baseEl).attr('name') )
                alreadyHasTooltip=true;
        });
        if(alreadyHasTooltip) return null; 
            
        var storeTooltip = storeTooltip || false;
        var margin = margin || 6;
        var input = $(baseEl);
        var span = $("<span class='tempTooltip' name='"+input.attr('name')+"'>" + msg + '</span>').addClass('tooltip');
        input.offsetParent().append(span);
        var par = input.offsetParent().offset();
        var off = input.position();
        off.left += (input.outerWidth(true) + margin + par.left);
        off.top += (((input.outerHeight() - span.outerHeight()) / 2) + par.top);
        span.offset(off);
        if (storeTooltip)
            input.data({assocTooltip: span});
        return {
            timeout: setTimeout(function() {
                span.fadeOut(2000, 'linear', function() {
                    $(this).remove();
                });
            }, timeout),
            el: span
        };
    }


    var form = $('.general_form');
    // gestione username
    function userSearch_success(data, textStatus, jqXHR) {
        if (data === '1') { // found
            showTooltip($(this), "Username already exists", 10, 2500);
        }
    }

    function userSearch_error() {
        console.log("error");
    }
    function userSearch_handler(context) {
        var that = context || this;
        $.ajax({
            success: userSearch_success,
            error: userSearch_error,
            url: 'liveUsernameSearch.php',
            dataType: 'html',
            context: that,
            data: {username: that.value}
        });
    }
    function confirmPassword_handler(el, storeTooltip) {
        if (form.find("input[name='password']").val() !== $(el).val()) {
            showTooltip($(el), "Passwords mismatch", 10, 2500, storeTooltip);
        }
    }
    function confirmBirthday_handler(el, storeTooltip) {
        try {
            var date = Date.parse(el.value);
            if (!date.between(Date.today().add({years: -120}), Date.today()))
                showTooltip($(el), "Wrong date or format incorrect", 10, 2500, storeTooltip);
        }
        catch (e)
        {
            showTooltip($(el), "Wrong date or format incorrect", 10, 2500, storeTooltip);
        }
    }
    function confirmEmail_handler(el, storeTooltip) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(el.value))
            showTooltip($(el), "Characters or format incorrect", 10, 2500, storeTooltip);
    }
    function confirmPhone_handler(el, storeTooltip) {
        var re = /^[0-9]{10,11}$/;
        if (!re.test(el.value))
            showTooltip($(el), "Format incorrect", 10, 2500, storeTooltip);
    }

    form.find("input[name='username']").focusout(function(e) {
        userSearch_handler(e.target);
    });
    // gestione password pre submit
    form.find("input[name='confirm_password']").focusout(function(e) {
        confirmPassword_handler(e.target);
    });
    form.find("input[name='email']").focusout(function(e) {
        confirmEmail_handler(e.target);
    });
    form.find("input[name='birthday']").focusout(function(e) {
        confirmBirthday_handler(e.target);
    });
    form.find("input[name='phone']").focusout(function(e) {
        confirmPhone_handler(e.target);
    });
// validazione della form
    form.find("#registration_button").click(function() {
        var failedCheck = 0;

        form.find('input').filter(function(index) {
            return !$(this).val() ? 1 : 0;
        }).each(function(i, el) {
            showTooltip($(el), "Field required", 10, 3500, true);
        }).end().filter(function(index) {
            return !$(this).val() ? 0 : 1;
        }).end().end().find("input[name='username']").each(function(i, el) {
            userSearch_handler(el, true);
        }).end().find("input[name='confirm_password']").each(function(i, el) {
            confirmPassword_handler(el, true);
        }).end().find("input[name='birthday']").each(function(i, el) {
            confirmBirthday_handler(el, true);
        }).end().find("input[name='email']").each(function(i, el) {
            confirmEmail_handler(el, true);
        }).end().find("input[name='phone']").each(function(i, el) {
            confirmPhone_handler(el, true);
        });
        form.find("input").filter(function(index, el) {
            var $el = $(el);
            if ($.hasData(el) && $el.data('assocTooltip'))
            {
                failedCheck++;
                return true;
            }
            else
                return false;
        }).first().focus();
        
        form.find("input").removeData();
        if (!failedCheck) {
            form.append($("<input id='hiddenSubmit' type='submit' name='submit' value='Register' />").css({display:'none'}));
            form.attr({
                method:'POST',
                url:'index.php?id=33'
            });
            form.find("input[name='birthday']").each(function(i,el){
                el.value=Date.parse(el.value).toString("yyyy-MM-dd");
            });
            form.find('#hiddenSubmit').click();
        }
        console.log();
});
});