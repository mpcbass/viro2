function parseGetVars()
{
    var args = new Array();
    var query = window.location.search.substring(1);
    if (query)
    {
        var strList = query.split('&');
        for (str in strList) {
            var parts = strList[str].split('=');
            args[unescape(parts[0])] = unescape(parts[1]);
        }
    }
    return args;
}
function error(jqXHR, textStatus, errorThrown) {
    console.log("ERROR - Ajax Request");
    console.log(jqXHR);
    console.log(textStatus);
    console.log(errorThrown);
}
/*function success(data, textStatus, jqXHR) {
    console.log(data);
    var cont = $('#content');
    cont.html(data);
    attachEvent(cont);
}*/
function success(data, textStatus, jqXHR) {
    var cont = $('#content');
    var grid=$('#content>*:not(#carousel_box)');
    grid.remove();
    cont.append(data);
    attachEvent(cont);
}
function gridRequest(data)
{
    data.id=parseGetVars()['id'];
    $.ajax('productWall.php', {
        dataType: 'html',
        success: success,
        error: error,
        data: data
    });
}
function applyFilters()
{
    var data = {};
    $(".filter_bar input:checked").each(
            function(i, el) {
                data[el.name] = el.value;
		data.filter = "active";
            });
    return data;
}
function pagerClick()
{
    var get=parseGetVars();
    console.log("button press");
    var data = applyFilters();
    if(get['m'])
        data['m']=get['m'];
    if(get['cat1'])
        data['cat1']=get['cat1'];
    if(get['cat2'])
        data['cat2']=get['cat2'];
    data.el = this.value;
    gridRequest(data);
}
 function initGrid(get)
{
    var data = applyFilters();
     if(get['s'])
        data['s']=get['s'];
    if(get['m'])
        data['m']=get['m'];
    if(get['cat1'])
        data['cat1']=get['cat1'];
    if(get['cat2'])
        data['cat2']=get['cat2'];
    data.el = 0;
    gridRequest(data);
}
function attachEvent(cont)
{
    cont.find('.prod_range button').click(pagerClick);
}
function addFilters()
{
    var data = applyFilters();
    data.el = 0;
    gridRequest(data);
}
$(document).ready(
        function() {
            // passo undefined se non provengo da una search 
            // se no rimando la stringa di ricerca
            initGrid(parseGetVars());
            $('.filter_bar input:checkbox').change(addFilters);
        });