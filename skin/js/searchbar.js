function search_success(data, textStatus, jqXHR) {
    try {
        console.log(data);
        var responseArray = JSON.parse(jqXHR.responseText);
        var list = $('#search_popup');
        if (list.length)
            list.empty();
        else {
            list = document.createElement('ul');
            list.id = "search_popup";
            list = $(list);
        }

        if (responseArray.length > 0) {
            for (var i = 0; i < responseArray.length; i++) {
                var aux = document.createElement('li');
                aux.innerHTML = responseArray[i];
                aux.onclick = function(e) {
                    e.stopPropagation();

                    $('#s').val(this.innerHTML);
                    $('#s').focus();
                    $(list).css('display', 'none');

                    return false;
                };
                list.append(aux);
            }
            $('#searchbarForm').append(list);
            list.css('display', 'block');
        }
        else {
            list.detach();
        }
    }
    catch (e) {
        console.log("Searchbar - Ajax Request Error");
    }
}
function search_error() {
    console.log("NOK");
}

$(document).ready(function() {

    $('#s').keyup(function() {
        var that = this;
        $.ajax('livesearch.php', {
            dataType: 'html',
            success: search_success,
            error: search_error,
            data: {s: that.value}
        });
    });
    $(document).click(function() {
        if ($('#search_popup').length)
            $('#search_popup').css('display', 'none');
    });



    /* other ui event 
    $('#s').keydown(function(e) {
        console.log(e.which);
        if (e.which === 40 || e.which === 38)
        {
            var $this = $(this);
            var curEl = $this.parents('form').find('ul li.currentOpt');
            if (curEl.length)
            {
                var newEl;
                if (e.which === 40)
                    newEl = curEl[0].nextSibling;
                else
                    newEl = curEl[0].previousSibling;
            }
            else
                newEl = $(this).parents('form').find('ul li:first-child');

            if (newEl.length)
            {
                $(newEl).addClass('currentOpt');
                if (curEl.length)
                    curEl.removeClass('currentOpt');
            }


            e.preventDefault();
            return false;
        }
    });*/


});

/*
 <ul id="search_popup">
 <li>cont</li>
 <li>cont</li>
 <li>cont</li>
 <li>cont</li>
 </ul>
 
 
 $(".myDiv").click(function(e) {
 e.stopPropagation(); // This is the preferred method.
 return false;        // This should not be used unless you do not want
 // any click events registering inside the div
 });
 
 
 */