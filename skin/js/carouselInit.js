
$(document).ready(function() {

    var slide = jQuery('#daCarousel');
    var carousel = slide.elastislide({
        speed: 400,
        minItems: 2,
        resize: '60%'
    });

    setInterval(function() {
        carousel.setCurrent((carousel.current + 1) % (carousel.itemsCount - 1));
    }, 3000);
});