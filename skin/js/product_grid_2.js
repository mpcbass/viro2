function error(jqXHR, textStatus, errorThrown) {
    console.log("ERROR - Ajax Request");
    console.log(jqXHR);
    console.log(textStatus);
    console.log(errorThrown);
}
function success(data, textStatus, jqXHR) {
    console.log(data);
    $('#content').html(data);
}


$(document).ready(
        function() {
            var tryNum = 50;
            var timer = setInterval(function() {
                var checked = $(".filter_bar");
                if (checked.length) {
                    clearInterval(timer);
                    var data = {};

                    checked = checked.find("input:checked");
                    checked.each(
                            function(i, el) {
                                data[el.name] = el.value;
                            });
                    data['el'] = 0;
                    $.ajax('productWall.php', {
                        dataType: 'html',
                        success: success,
                        error: error,
                        data: data
                    });
                }
                else {
                    if (tryNum >= 0)
                        tryNum--;
                    else
                        clearInterval(timer);
                    console.log('Still not loaded');
                }

                $('.prod_range button').click(
                        function() {
                    console.log("button press");
                            var data = {};

                            checked = checked.find("input:checked");
                            checked.each(
                                    function(i, el) {
                                        data[el.name] = el.value;
                                    });
                            data['el'] = this.value;
                            $.ajax('productWall.php', {
                                dataType: 'html',
                                success: success,
                                error: error,
                                data: data
                            });
                        });

            }, 200);
        });