$(document).ready(function() {
    var form = $('form#checkout-form');

    function showTooltipBottom(baseEl, msg, margin, timeout, storeTooltip) {
        var storeTooltip = storeTooltip || false;
        var margin = margin || 6;
        var input = $(baseEl);
        var span = $('<span>' + msg + '</span>').addClass('tooltip bottom');
        input.offsetParent().append(span);
        var par = input.offsetParent().offset();
        var off = input.position();
        off.left += par.left;
        off.top += (input.outerHeight() + margin + par.top);
        span.offset(off);
        if (storeTooltip)
            input.data({assocTooltip: span});
        return {
            timeout: setTimeout(function() {
                span.fadeOut(2000, 'linear', function() {
                    $(this).remove();
                });
            }, timeout),
            el: span
        };
    }

    function confirmEmail_handler(el, storeTooltip) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(el.value))
            showTooltipBottom($(el), "Characters or format incorrect", 10, 2500, storeTooltip);
    }
    function confirmPhone_handler(el, storeTooltip) {
        var re = /^[0-9]{10,11}$/;
        if (!re.test(el.value))
            showTooltipBottom($(el), "Format incorrect", 10, 2500, storeTooltip);
    }

    /* UI event */
    form.find("input[name='payment_method']").change(function() {
        if ($(this).val() === 'bacs')
        {
            var $cont = $(this).parents('#payment');
            $cont.find('#payment_method_form').find('#cred_card_first_name').val(form.find('#billing_first_name').val()).end()
                    .find('#cred_card_last_name').val(form.find('#billing_last_name').val()).end().show();
            $cont.find("input[type='text']").first().focus();
        }
        else
            form.find('#payment_method_form').hide();
    });
    form.find("input#shiptobilling-checkbox").change(function(e) {
        if (e.target.checked === false)
            $('.shipping_address').show();
        else
            $('.shipping_address').hide();
    });
    form.find("input#billing_postcode").focusout(function(e) {
        var aux = $(e.target).val();
        var reg = /^[0-9]{2,5}$/;
        if (aux.length && !reg.test(aux))
            showTooltipBottom($(e.target), "Invalid format", 10, 2500);
    });

    form.find("input#shipping_postcode").focusout(function(e) {
        var aux = $(e.target).val();
        var reg = /^[0-9]{2,5}$/;
        if (aux.length && !reg.test(aux))
            showTooltipBottom($(e.target), "Invalid format", 10, 2500);
    });
    form.find("input#billing_email").focusout(function(e) {
        confirmEmail_handler(e.target);
    });
    form.find("input#billing_phone").focusout(function(e) {
        confirmPhone_handler(e.target);
    });
    
    form.find('#billing_block input').keyup(function(e) {
        try {
            var selector = e.target.id.replace('billing', 'shipping');
            form.find("#"+selector).val(e.target.value);
        }
        catch (e)
        {
            console.log('Error in auto-compiling shipping form');
        }
    });
    form.find('#billing_block select').change(function(e) {
        try {
            var selector = e.target.id.replace('billing', 'shipping');
            form.find("#"+selector).val(e.target.value);
        }
        catch (e)
        {
            console.log('Error in auto-compiling shipping form');
        }
    });

    /* Startup*/
    $('.shipping_address').hide();
    $('#payment_method_form').hide();
});
