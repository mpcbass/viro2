$(document).ready(function() {
    function updateCartOnDB(prodsTable, subtot)
    {
        var cart = [];
        var deleteCart = true;

        prodsTable.find('tbody tr').each(function(i, el) {
            deleteCart = false;
            try
            {
                var aux = {};
                var id = $(el).find('.product-thumbnail a').attr('href');
                id = id.match(/id_prod=[0-9]+/)[0];
                id = id.replace(/[^0-9]*/, "");
                aux.id = id;
                aux.qty = ($(el).find('select').val());
                cart.push(aux);
            }
            catch (e)
            {
                console.log('Unable to update Database');
            }
        });

        //send update
        console.log('Updating Database');

        $.ajax({
            type: 'POST',
            url: 'updateCartAjax.php',
            data: {
                data: JSON.stringify(cart),
                deleteCart: deleteCart,
                tot: subtot
            },
            success: function(data) {
                console.log("DB->SUCCESS");
                console.log(data);
                $('.cart_totals .shipping td')[0].innerHTML = '&euro;' + JSON.parse(data)['shipping'].toFixed(2);
            },
            error: function(e) {
                console.log("DB->FAIL");
            }
        });
    }

    function recalcSubtotals(prodsTable)
    {
        try
        {
            var subtot = 0;
            prodsTable.find('.product-subtotal .amount').each(function(i, el) {
                subtot += Number(el.innerText.replace(/[^0-9\.]+/g, "")) || 0;
            });

            var cart = $('.cart_totals');
            cart.find('.cart-subtotal .amount').html('&euro;' + (subtot).toFixed(2)) || 0;

            var shipping = cart.find('.shipping td')[0].innerText || "0";
            shipping = Number(shipping.replace(/[^0-9\.]+/g, ""));

            cart.find('.total .amount').html('&euro;' + (subtot + shipping).toFixed(2));

            // update database
            updateCartOnDB(prodsTable, subtot);
        }
        catch (e)
        {
            console.log('Unable to update Totals');
        }
    }
    
    function refreshCart(event){
        try {
            var qty = event.target.value;
            var itemRow = $(event.target).parents('.art_table_item');
            var price = itemRow.find('.product-price .amount')[0].innerText;
            price = Number(price.replace(/[^0-9\.]+/g, ""));
            itemRow.find('.product-subtotal .amount').html('&euro;' + (price * qty).toFixed(2));

            recalcSubtotals(itemRow.parents('table'));
        }
        catch (e)
        {
            console.log('Unable to update Cart');
        }
    }
    
    // evento triggerato alla variazione di quantità dei prodotti inline
    $('.product-quantity select').change(refreshCart);
// evento triggerato alla rimozione di un obj dal carrello
    $('.shop_table .product-remove .remove').click(function(event) {
        var el = $(event.target);
        var table = el.parents('table');
        el.parents('tr').remove();
        table.find('tfoot').remove();
        recalcSubtotals(table);
    });
    
    //init
    recalcSubtotals($('.shop_table'));
});
