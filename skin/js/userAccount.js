/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function() {
    //vars
    var orderBlock;

    // utils
    function cacheOrders(content)
    {
        if ($(content).find('.resumeOrder').length)
        {
            
            orderBlock = $(content).find('.deleteButtonRow').remove().end().html();
        }
    }
    function systemMessageFadeout() {
        $this = $(this);
        setTimeout(function() {
            $this.slideUp(2500, function() {
                $this.remove();
            });
        }, 3000);
    }
    function systemMessage(msg) {
        var $cont = $('#content');
        if (!$cont.find('.page-header').length)
        {
            $cont.prepend("<header class='page-header'><h1 class='page-title sysMessage'>" + msg + "</h1></header>")
                    .children('header').map(systemMessageFadeout);
        }
        else
        {
            $cont.find('.page-header').html("<h1 class='page-title sysMessage'>" + msg + "</h1>")
                    .children('header').map(systemMessageFadeout);
        }
    }

    /* ui events */
    $('#canc_orders').click(function() {
        
        if(!$('#content').find('.resumeOrder').length)
            $('#content').html(orderBlock);
        var $button = $('.deleteOrderButton');
        if (!$button.length)
        {
            $('.resumeOrder:not(.deliveredOrder) tbody').append("<tr style='display:none' class='deleteButtonRow'/>").children('.deleteButtonRow').append("<td colspan='5' style='padding:0'/>")
                    .children().append("<span class='deleteOrderButton' style='display:none'>DELETE ORDER</span>");
            $('.deleteButtonRow').slideDown({
                'duration': 500,
                'complete': function() {
                    $('.deleteOrderButton').fadeIn(300);
                }
            });

            $('.deleteOrderButton').click(function(e) {

                function uiMessage(msg) {
                    var $cont = $('#content');
                    if (!$cont.find('.page-header').length)
                    {
                        $cont.prepend("<header class='page-header'><h1 class='page-title sysMessage'>" + msg + "</h1></header>").children('header').map(function() {
                            $this = $(this);
                            setTimeout(function() {
                                $this.slideUp(2500);
                                $('#canc_orders').click();
                                $(e.target).parents('.resumeOrder').slideUp(2500, function() {
                                    $(this).remove()
                                });
                            }, 3000);
                        });
                    }
                    else
                    {
                        $cont.find('.page-header').html("<h1 class='page-title sysMessage'>" + msg + "</h1>").children('header').map(function() {
                            $this = $(this);
                            setTimeout(function() {
                                $this.slideUp(2500);
                                $('#canc_orders').click();
                            }, 3000);
                        });
                    }
                }
                $butt = $(e.target);
                var ord = $butt.parents('.resumeOrder').find('.orderNum').text();
                ord = ord.match(/[0-9]+/);
                if (ord.length)
                {
                    ord = ord[0];
                    $.ajax({
                        success: uiMessage,
                        data: {del_ord: ord},
                        type: 'GET',
                        url: 'deleteOrderAccount.php'
                    });
                }


            });
        }
        else
        {
            $button.fadeOut(300, function() {
                $(this).parents('.deleteButtonRow').slideUp(500, function() {
                    $(this).remove();
                });
                ;
            });
        }
    });
    $('#change_pwd').click(function() {
        function error() {
            console.log('Error change PWD');
        }
        function success(data, textStatus, jqXHR) {
            $('#content').html(data);
        }
        cacheOrders($('#content'));
        $.ajax({
            url: 'skin/dtml/b_modify_password.html',
            success: success,
            error: error
        });
    });
    $('#del_user').click(function() {
        function error() {
            console.log('Error delete Account');
        }
        function success(data, textStatus, jqXHR) {
            $('#content').html(data);
        }
        cacheOrders($('#content'));
        $.ajax({
            url: 'skin/dtml/b_delete_account.html',
            success: success,
            error: error
        });
    });
    $('#mod_user').click(function() {
        cacheOrders($('#content'));
    });

    /* init */
    $('#canc_orders').data('del_mode', false);

    /* parse GET */
    var get = location.search.match(/[^&?]+/g);
    while (get.length) {
        var aux = get.shift().match(/[^=]+/g);
        get[aux[0]] = aux[1];
    }
    if (get['mex'])
    {
        systemMessage(get['mex']);
    }
    console.log(get);


});