jQuery('html').removeClass('no-js').addClass('js');

//	Hide product images
	var i = 0; //initialize
	var int=0; //Internet Explorer Fix
	jQuery(window).bind("load", function() { //The load event will only fire if the entire page or document is fully loaded
		var int = setInterval("doThis(i)",400); //500 is the fade in speed in milliseconds 		
	});

	function doThis() {
		var imgs = jQuery('#content ul.products li').length; //count the number of images on the page
		if (i >= imgs) { //Loop the images
			clearInterval(int); //When it reaches the last image the loop ends
		}
		jQuery('img:hidden').eq(0).fadeIn(400); //fades in the hidden images one by one
		i++; //add 1 to the count
	}

jQuery(document).ready(function($) {

//	Hide Product Images
	$('#content ul.products li img').hide();

//	Responsive Menu
	$(function(){
		$("<select />").appendTo("#menu");
		
		$("<option />", {
		   "selected": "selected",
		   "value"   : "",
		   "text"    : "Navigate..."
		}).appendTo("nav select");
		
		// Populate dropdown with menu items
		$(".sf-menu a").each(function() {
			var el = $(this);
			$("<option />", {
				"value"   : el.attr("href"),
				"text"    : el.text()
			}).appendTo("nav select");
		});
		
		$("nav select").change(function() {
		  window.location = $(this).find("option:selected").val();
		});
	});

//	Shopping bag
	$("#shopping-bag").click(function(){
		window.location=$(this).find("a").attr("href");
		return false;
	});
		
//	Superfish 				
	$('ul.sf-menu').superfish({
		disableHI:true,          
		dropShadows: false,
		speed: 'fast',
		delay: 0,   
		autoArrows: false
	});	
	
//	Widget slidedown menu
	$('.widget_nav_menu li.current-menu-parent > .sub-menu', this).prev().addClass('active');
	$('.widget_nav_menu li a', this).click( function(e) {
			e.stopImmediatePropagation();
	
			var theElement = $(this).next();
			var parent = this.parentNode.parentNode;
	
	
			if($(parent).hasClass('sub-menu')) {
				if(theElement[0] === undefined) {
					window.location.href = this.href;
				}
				$(theElement).slideToggle('normal', function() {
					if ($(this).is(':visible')) {
						$(this).prev().addClass('active');
					}
					else {
						$(this).prev().removeClass('active');
					}    
				});
				return false;
			}
			else {
				if(theElement.hasClass('sub-menu') && theElement.is(':visible')) {
					if($(parent).hasClass('collapsible')) {
						$('.sub-menu:visible', parent).first().slideUp('normal', 
						function() {
							$(this).prev().removeClass('active');
						}
					);
					return false;  
				}
				return false;
			}
			if(theElement.hasClass('sub-menu') && !theElement.is(':visible')) {         
				$('.sub-menu:visible', parent).first().slideUp('normal', function() {
					$(this).prev().removeClass('active');
				});
				theElement.slideDown('normal', function() {
					$(this).prev().addClass('active');
				});
				return false;
			}
		}
	});
	 
});

 