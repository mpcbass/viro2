jQuery(document).ready(function($) {
	$("#loadplace").hide();

	$('.submit').click(function(e){	
		e.preventDefault();  
		$("#sidebar .widget_loginformwidget").slideUp('slow', function() {
			$("#loginForm").hide();
			$("#loadplace").show();
    	});
		$("#sidebar .widget_loginformwidget").delay(1000).slideDown('slow');
		return false;	
	});
	
	$(".try-again a").click(function() {
		$("#sidebar .widget_loginformwidget").slideUp('slow', function() {
			$("#loginForm").show();
			$("#loadplace").hide();
    	});
		$("#sidebar .widget_loginformwidget").delay(1000).slideDown('slow',  function() {
			$("#loadplace").hide();
    	});		
		return false;
	});
	
});