<?php

require_once 'include/utilities.inc.php';
require_once 'include/template2.inc.php';
if (isset($_GET['add'])) {
    if($_GET['position'] == 0){
        $query = "SELECT MAX(posizione) AS 'pos' FROM 6_slide WHERE id_page ={$_GET['cat']}";
        $ris = getResult($query);
        if(isset($ris[0]['pos'])){
            $ris[0]['pos']++;
            $query = "INSERT INTO 6_slide VALUES('','skin/images/slides/{$_GET['name']}','{$_GET['alt']}',{$ris[0]['pos']},{$_GET['cat']});";
        }else{
            $query = "INSERT INTO 6_slide VALUES('','skin/images/slides/{$_GET['name']}','{$_GET['alt']}',1,{$_GET['cat']});";
        }
    } else{
        $query = "INSERT INTO 6_slide VALUES('','skin/images/slides/{$_GET['name']}','{$_GET['alt']}',{$_GET['position']},{$_GET['cat']});";
    }
        queryInsert($query);
    header('location:index.php?mex=slide correctly added!');
} else if (isset($_GET['del'])) {
    $query = "DELETE FROM 6_slide WHERE id = {$_GET['slide']};";
    queryInsert($query);
    header("location:index.php?mex=slides correctly deleted!");
} else {
    $main = new Template('skin/dtml/t_frame_private.html');
    $nav_bar = new Template('skin/dtml/b_main_nav_bar.html');
    $side_menu = new Template('skin/dtml/b_side_menus.html');
    $form = new Template('skin/dtml/b_manage_slide.html');
    $query = "SELECT * FROM 5_category WHERE 1;";
    $ris = getResult($query);
    $cat_page = array();
    foreach ($ris as $k => $v) {
        $query = "SELECT * FROM 1_service WHERE file_name LIKE'{$v['descr']}.php'";
        $ris1 = getResult($query);
        $cat_page[$k]['id'] = $ris1[0]['id'];
        $cat_page[$k]['descr'] = $v['descr'];
    }
    $form->setContent('cat_list', $cat_page);
    $query = "SELECT * FROM 6_slide;";
    $ris = getResult($query);
    $form->setContent('slide_list', $ris);
    $this_page = cercaPaginadaDescrizione('slides');
    $form->setContent('id_page', $this_page);
    $form->setContent('id_page1', $this_page);
    $side_menu->setContent('page_slide', $this_page);
    $side_menu->setContent('page_cat', cercaPaginadaDescrizione('add_cat'));
    $side_menu->setContent('page_muser', cercaPaginadaDescrizione('modify_user'));
    $side_menu->setContent('page_adprod', cercaPaginadaDescrizione('manage_prod'));
    $side_menu->setContent('page_user', cercaPaginadaDescrizione('manage_user'));
    $side_menu->setContent('page_order', cercaPaginadaDescrizione('manage_order'));
    $side_menu->setContent('page_mprod', cercaPaginadaDescrizione('modify_product'));
    $ris = menu('header_admin');
    $nav_bar->setContent('main_menu', $ris);


    $main->setContent('inner_container', $form->get());
    $main->setContent('side_menu', $side_menu->get());
    $main->setContent('main_nav_bar', $nav_bar->get());
    $main->close();
}
?>
