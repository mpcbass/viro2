<?php

session_start();

$valori = ""; #variabile contenente i valori dei campi per la query SQL

require_once "include/template2.inc.php";
require_once "include/utilities.inc.php";

unset($_GET['id']);
#funzione che restituisce 0 se non è stato settato nessun campo, 1 se tutti i 
#+campi sono corretti e una stringa contenente i nomi dei campi errati nel caso ce ne siano

function verifica_campi() {
    global $valori;
    $campi_errati = "";

    if (!isset($_POST['submit'])) {
        return 0;
    }
    $_POST = avoid_sql_injection($_POST);
    if ($_POST['username'] != NULL) {
        $valori = "'{$_POST['username']}',";
    } else {
        $campi_errati .="%20username";
    }
    if ($_POST['password'] != NULL && $_POST['confirm_password'] != NULL && $_POST['password'] == $_POST['confirm_password']) {
        $valori .= "'" . cripta_password($_POST['password'], $_POST['username']) . "',";
    } else {
        $campi_errati .="%20password confirm_password";
    }
    if ($_POST['name'] != NULL) {
        $valori .= "'{$_POST['name']}',";
    } else {
        $campi_errati .="%20name";
    }
    if ($_POST['surname'] != NULL) {
        $valori .= "'{$_POST['surname']}',";
    } else {
        $campi_errati .="%20surname";
    }
    if ($_POST['birthday'] != NULL) {
        if (preg_match('/[1-2][09][0-9][0-9][\-\/][0-1][0-9][\-\/][0-3][0-9]/', $_POST['birthday'])) {
            $birth = preg_replace('/[\-\/]/', ',', $_POST['birthday']); #necessaria per la funzione STR_TO_DATE
            $valori .= "STR_TO_DATE('{$birth}','%Y,%m,%d'),";
        } else {
            $campi_errati .= "%20date_of_birth";
        }
    } else {
        $campi_errati .= "%20date_of_birth";
    }
    if ($_POST['email'] != NULL) {
        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $campi_errati .= "%20email";
        }
        $valori .= "'{$_POST['email']}',";
    } else {
        $campi_errati .= "%20email";
    }
    if ($_POST['phone'] != NULL) {
        $valori .= "'{$_POST['phone']}'";
        if (!preg_match('/[0-9]{10,11}/', $_POST['phone'])) {
            $campi_errati .= "%20phone";
        }
    } else {
        $campi_errati .= "%20phone";
    }
    #valore di ritorno
    if ($campi_errati != "") {
        return $campi_errati;
    }
    return 1;
}

$main = new Template('skin/dtml/t_frame-public.html');
$form = new Template('skin/dtml/b_add_user.html');
$nav_bar = new Template('skin/dtml/b_main_nav_bar.html');


#da prevedere poichè chiunque potrebbe accedere alla pagina sapendo il nome
if (isLogged() && !isAdmin()) {
    header("location:index.php");
}
#settare la pagina da amministratore
else if (isAdmin()) {
    $ris = menu('header_admin');
}
#settare la pagina da t-user
else {
    $ris = menu('header');
    $search_bar = new Template('skin/dtml/b_search_bar.html');
    $nav_bar->setContent('search_bar', $search_bar->get());
    $login = new Template('skin/dtml/b_side_login.html');
    $login->setContent('page', 'index.php');
    $main->setContent('login', $login->get());
    #########
    #carrello
    $carrello = carrello();
    if ($carrello != NULL) {
        $top_cart = new Template('skin/dtml/b_cart.html');
        $top_cart->setContent('price_amount', $carrello[0]);
        $top_cart->setContent('item_num', $carrello[1]);
        $top_cart->setContent('item_plural', $carrello[2]);
        $main->setContent('cart', $top_cart->get());
    }else{
        $top_cart = new Template('skin/dtml/b_cart.html');
    $top_cart->setContent('price_amount', '0.00');
    $top_cart->setContent('item_num', '0');
    $top_cart->setContent('item_plural', "");
    $top_cart->setContent('id_cart', cercaPaginadaDescrizione('cart'));
    $main->setContent('cart', $top_cart->get());
    }
    #########
}

#controllo errori
if (isset($_GET['err'])) {
    $campi = explode("%20", $_GET['err']);
    $string = "recheck the following fields: ";
    foreach ($campi as $k => $v) {
        $string.="$v <br/>";
    }
    $form->setContent('error_message', "<p>" . $string . "</p>");
    unset($_GET['err']);
    #
    foreach ($_GET as $k => $v) {
        $form->setContent($k, $v);
    }
}

$ris2 = verifica_campi();
#registrare utente
if ($ris2 === 1) {
    $query = "INSERT INTO 1_user VALUES ('',{$valori});";
    if (queryInsert($query)) {
        $query = "SELECT MAX( 1_user.id ) AS 'uid', 1_group.id AS 'gid'FROM 1_user, 1_group WHERE 1_group.descr = 'p_user'";
        $ids = getResult($query);
        $query = "INSERT INTO 1_user_group VALUES({$ids[0]['uid']},{$ids[0]['gid']});";
        queryInsert($query);
        header("location:registration.php?regok=");
    } else {
        header("location:error.php?e_type=add_user_fail%20tutti");
    }
}
#visualizzazione pagina
else {
    if ($ris2 === 0) {
        $nav_bar->setContent('main_menu', $ris);
        $main->setContent('main_nav_bar', $nav_bar->get());
        if (isset($_GET['regok'])) {
        #registrazione effettuata
            header("refresh:3;url=index.php");
            $main->setContent('inner_container', "<h1>REGISTRATION SUCCESSFUL</h1><br /><h3>You will be redirected in 3 seconds</h3>");
            $main->close();
            break;
        }
        $main->setContent('inner_container', $form->get());
        $scripts = array();
        $scripts[] = "datejs.js";
        $scripts[] = "register-form.js";
        $main->setContent('scripts', $scripts);
        $main->close();
    } else {
        #richiamare la pagina con gli errori trovati
        $campi_compilati = "";
        foreach ($_POST as $k => $v) {
            if ($k != 'password' && $k != 'confirm_password') {
                $campi_compilati .= "&{$k}={$v}";
            }
        }
        header("location:error.php?e_type=add_user_fail%20" . $ris2 . $campi_compilati);
    }
}
?>
