<?php

session_start();

require_once "include/dbms.inc.php";
require_once 'include/template2.inc.php';
require_once 'include/accessMng.inc.php';
require_once 'include/utilities.inc.php';

if (isLogged()) {
    header('location:index.php');
} else {
    if (isset($_POST) && isset($_POST['username']) && isset($_POST['password'])) {
        $_POST = avoid_sql_injection($_POST);
        if (verificaCredenziali($_POST['username'], $_POST['password'])) {
            cambiaSettaggiSession($_POST['username']);
            if (isAdmin()) {
                $location = "location:index.php";
            } else if (isset($_POST['page'])) {
                $location = "location: index.php?id=" . $_POST['page'];
            } else {
                $location = "location: index.php";
            }
            header($location);
        } else {
            header('location:error.php?e_type=log_fail');
        }
    } else {
        $main = new Template('skin/dtml/t_frame-public.html');
        $login = new Template('skin/dtml/b_login.html');
        $nav_bar = new Template('skin/dtml/b_main_nav_bar.html');
        $search_bar = new Template('skin/dtml/b_search_bar.html');

        if (isset($_GET['err'])) {
            $login->setContent('error_message', "<p>" . $_GET['err'] . "</p>");
        }

        $ris = menu('header');

        $nav_bar->setContent('main_menu', $ris);
        $nav_bar->setContent('search_bar', $search_bar->get());
        #########
        #carrello
        $carrello = carrello();
        if ($carrello != NULL) {
            $top_cart = new Template('skin/dtml/b_cart.html');
            $top_cart->setContent('price_amount', $carrello[0]);
            $top_cart->setContent('item_num', $carrello[1]);
            $top_cart->setContent('item_plural', $carrello[2]);
            $main->setContent('cart', $top_cart->get());
        } else {
            $top_cart = new Template('skin/dtml/b_cart.html');
            $top_cart->setContent('price_amount', '0.00');
            $top_cart->setContent('item_num', '0');
            $top_cart->setContent('item_plural', "");
            $top_cart->setContent('id_cart', cercaPaginadaDescrizione('cart'));
            $main->setContent('cart', $top_cart->get());
        }
        #########
        $main->setContent('carosello', "");
        $main->setContent('inner_container', $login->get());
        $main->setContent('main_nav_bar', $nav_bar->get());
        $main->close();
    }
}
?>
