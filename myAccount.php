<?php
session_start();

require_once 'include/template2.inc.php';
require_once 'include/utilities.inc.php';
require_once 'include/dbms.inc.php';

if(isset($_GET['id'])){
    $id_pagina = $_GET['id'];
    unset($_GET['id']);
}
else{
    $id_pagina = cercaPaginadaDescrizione('myAccount');
}

$main = new Template('skin/dtml/t_frame-public.html');
$nav_bar = new Template('skin/dtml/b_main_nav_bar.html');
$search_bar = new Template('skin/dtml/b_search_bar.html');
$menu_lat = new Template('skin/dtml/b_side_myaccount.html');
$home = new Template('skin/dtml/b_order_myaccount.html');

$user_greetings=new Template('skin/dtml/b_side_user_greetings.html');
$user_greetings->setContent('username', $_SESSION['username']);
$main->setContent('user', $user_greetings->get() );

$carrello = carrello();
if($carrello != NULL){
    $top_cart = new Template('skin/dtml/b_cart.html');
    $top_cart->setContent('price_amount', $carrello[0]);
    $top_cart->setContent('item_num', $carrello[1]);
    $top_cart->setContent('item_plural', $carrello[2]);
    $top_cart->setContent('id_cart', cercaPaginadaDescrizione('cart'));
    $main->setContent('cart', $top_cart->get());
}else{
    $top_cart = new Template('skin/dtml/b_cart.html');
    $top_cart->setContent('price_amount', '0.00');
    $top_cart->setContent('item_num', '0');
    $top_cart->setContent('item_plural', "");
    $top_cart->setContent('id_cart', cercaPaginadaDescrizione('cart'));
    $main->setContent('cart', $top_cart->get());
}

$query = "SELECT id FROM 1_user WHERE username = '{$_SESSION['username']}'";
$id_user = getResult($query);
$query = "SELECT 7_order.*, 7_order_detail.total AS 'total_prod',qty,id_prod FROM 7_order,7_order_detail WHERE n_ord = id_ord AND id_user = {$id_user[0]['id']} AND state = 'delivering' ORDER BY date DESC;";
$ris = getResult($query);
$home->setContent('ordini_non_consegnati', $ris);
$query = "SELECT 7_order.*, 7_order_detail.total AS 'total_prod',qty,id_prod FROM 7_order,7_order_detail WHERE n_ord = id_ord AND id_user = {$id_user[0]['id']} AND state = 'delivered' ORDER BY date DESC;";
$ris = getResult($query);
$home->setContent('ordini_consegnati', $ris);

$ris = menu('header');
$nav_bar->setContent('main_menu', $ris);
$search_bar->setContent('script', cercaPaginadaDescrizione('search'));
$nav_bar->setContent('search_bar', $search_bar->get());
$main->setContent('main_nav_bar',$nav_bar->get());
$main->setContent('social', $menu_lat->get());
$main->setContent('inner_container', $home->get());

$scripts=array('userAccount.js');
$main->setContent('scripts', $scripts);

$main->close();
?>
