<?php

session_start();
require_once 'include/dbms.inc.php';
require_once 'include/utilities.inc.php';
require_once 'include/template2.inc.php';

$this_page = cercaPaginadaDescrizione('modify_user');

if (isset($_GET['search'])) {
    $main = new Template('skin/dtml/t_frame_private.html');
    $side_menu = new Template('skin/dtml/b_side_menus.html');
    $nav_bar = new Template('skin/dtml/b_main_nav_bar.html');
    $form = new Template('skin/dtml/b_mods_user.html');

    $form->setContent('id_page', $this_page);
    $query = "SELECT * FROM 1_user WHERE username = '{$_GET['username']}';";
    $ris = getResult($query);
    if ($ris <= 0) {
        header("location:index.php?id=".$this_page."&err=specified username does not exists!");
    } else {
        foreach ($ris[0] as $k => $v) {
            $form->setContent($k, $v);
        }
    }
    if (isset($_GET['err'])) {
        $form->setContent('error_message', $_GET['err']);
    }
    $ris = menu('header_admin');
    $nav_bar->setContent('main_menu', $ris);

    $side_menu->setContent('page_slide', cercaPaginadaDescrizione('slides'));
    $side_menu->setContent('page_order', cercaPaginadaDescrizione('manage_order'));
    $side_menu->setContent('page_cat', cercaPaginadaDescrizione('add_cat'));
    $side_menu->setContent('page_adprod', cercaPaginadaDescrizione('manage_product'));
    $side_menu->setContent('page_user', cercaPaginadaDescrizione('manage_user'));
    $side_menu->setContent('page_muser', $this_page);

    $main->setContent('side_menu', $side_menu->get());
    $main->setContent('main_nav_bar', $nav_bar->get());
    $main->setContent('inner_container', $form->get());
    $main->close();
} else if (isset($_GET['mods'])) {
    if (isset($_GET['confirm_pwd'])) {
        if ($_GET['password'] != $_GET['confirm_pwd']) {
            header("location:index.php?id=" . $this_page . "&err=Password do not correspond!&search=");
        }
    }
    if (!preg_match('/[\w]{4}[\/\-]/', $_GET['date_of_birth'])) {
        $_GET['date_of_birth'] = preg_replace("/([\w]+)([\/\-])([\w]+)([\/\-])([\w]+)/", "\$1,\$3,\$5", $_GET['date_of_birth']);
        $_GET['date_of_birth'] = "(SELECT STR_TO_DATE('{$_GET['date_of_birth']}','%d,%m,%Y'))";
    }
    else{
        $_GET['date_of_birth'] = "'{$_GET['date_of_birth']}'";
    }
    $query = "UPDATE 1_user SET date_of_birth={$_GET['date_of_birth']}, password = '{$_GET['password']}',email = '{$_GET['email']}',phone = '{$_GET['phone']}',name='{$_GET['name']}',surname='{$_GET['surname']}' 
        WHERE id = {$_GET['ID']};";
    if (queryInsert($query)) {
        header("location:index.php?mex=user updated successfully!");
    } else {
        header("location:index.php?id=" . $this_page . "&err=parameters not valid!");
    }
} else {
    $main = new Template('skin/dtml/t_frame_private.html');
    $side_menu = new Template('skin/dtml/b_side_menus.html');
    $nav_bar = new Template('skin/dtml/b_main_nav_bar.html');
    $form = new Template('skin/dtml/b_search_user.html');

    $form->setContent('id_page', $this_page);
    if (isset($_GET['err'])) {
        $form->setContent('error_message', $_GET['err']);
    }
    $ris = menu('header_admin');
    $nav_bar->setContent('main_menu', $ris);

    $side_menu->setContent('page_slide', cercaPaginadaDescrizione('slides'));
    $side_menu->setContent('page_order', cercaPaginadaDescrizione('manage_order'));
    $side_menu->setContent('page_cat', cercaPaginadaDescrizione('add_cat'));
    $side_menu->setContent('page_adprod', cercaPaginadaDescrizione('manage_prod'));
    $side_menu->setContent('page_user', cercaPaginadaDescrizione('manage_user'));
    $side_menu->setContent('page_muser', $this_page);
    $side_menu->setContent('page_mprod', cercaPaginadaDescrizione('modify_product'));

    $main->setContent('side_menu', $side_menu->get());
    $main->setContent('main_nav_bar', $nav_bar->get());
    $main->setContent('inner_container', $form->get());
    $main->close();
}
?>
