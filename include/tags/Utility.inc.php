<?php

require_once 'include/utilities.inc.php';

Class Utility extends TagLibrary{
    function inserisciScripts($name, $data, $pars){
        $content = "";
	foreach($data as $k => $v){
	  $content .= "<script src='skin/js/".$v."'></script>";
          
	}
        return $content;
    }
    function visualizzaCat($name,$data,$pars){
        $content = "";
        foreach ($data as $k => $v){
            $content .= "<option value='{$v['id']}'>{$v['descr']}</option>";
        }
        return $content;
    }
    function visualizzaSlides($name,$data,$pars){
        $content = "";
        foreach ($data as $k => $v){
            $nome = preg_replace('/^([\/\w]+)\/([\w\.]+)$/', "\$2", $v['path']);
            $content .= "<option value='{$v['id']}'>{$nome}</option> ";
        }
        return $content;
    }
    function visualizzaDeliveryCosts($name,$data,$pars){
        $content = "";
        foreach ($data as $k => $v){
            $content .= "<option value={$v['id']}>{$v['descr']}, ".number_format($v['price'],2)."</option>";
        }
        return $content;
    }
    
    function visualizzaCarosello($name,$data,$pars){
        $content = "";
        $pre = "";
        $post = "";
        if(count($data) > 0){
            $pre = "<ul id='daCarousel' class='elastislide-list'>";
            $post = "</ul>";
        }
        $id_page_product = cercaPaginadaDescrizione('product');
        foreach($data as $k => $v){
            $content .= "<li><a href='index.php?id=".$id_page_product."&amp;id_prod={$v['id']}'>
                <span class='onsale'>{$v['discount']}% Off</span> 
                <img class='gallery' src='{$v['image']}' alt='{$v['alt_text']}' title='{$v['name']}'>   
                <div class='infoFilterContainer'>
                    <div class='single_star'>{$v['rating']}</div>
                    <h3>{$v['name']}</h3>
                    <span class='price'>€ {$v['price']}</span>
                </div>
            </a>
        </li>";
        }   
        return $pre.$content.$post;
    }
    
    function formattaMessaggio($name,$data,$pars){
        return"<p class='{$pars['classe']}'>{$data}</p><h4 id='timer'>you will be redirected to home in <span id='timerCount'></span></h4>";
    }
    
    function visualizzaGruppi($name,$data,$pars){
        $content = "";
        foreach($data as $k => $v){
            $content .= "<option value={$v['id']}>{$v['descr']}</option>";
        }
        return $content;
    }
    function optionProdotti($name,$data,$pars){
        $content = "";
        foreach($data as $k => $v){
            $content .= "<option value={$v['id']}>{$v['name']}&nbsp;&nbsp;#&nbsp;{$v['id']}</option>";
        }
        return $content;
    }
    function tabellaCheckout($name,$data,$pars){
        $content = "";
        foreach($data as $k => $v){
            $content .= "<tr class = 'checkout_table_item'>
                        <td class='product-name'>{$v['name']}</td>
                        <td class='product-quantity'>".number_format($v['qty_ord'])."</td>
                        <td class='product-total'><span class='amount'>&euro; ".number_format($v['qty_ord']*$v['price'],2)."</span></td>
                    </tr>";
        }
        return $content;
    }
}
?>
