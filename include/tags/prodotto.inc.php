<?php

Class prodotto extends TagLibrary{
    function visualizzaGriglia($name,$data,$pars){
        $css_first = $pars['firstclass'];
        $css_last = $pars['lastclass'];
        $css_normal = $pars['normalclass'];
        $lungh_riga = $pars['length'];
        $num_max_el = $pars['num'];
        $content = "";
        $i = 0;
        #########################
        #caso di product_list.php
        if($data == 0){
            return "NON CI SONO PRODOTTI CHE SODDISFANO I CRITERI DI RICERCA SELEZIONATI!<br/>";
        }
        #########################
        if(count($data) > 0){
            $content .= "<ul class='products'>";
            foreach($data as $k => $v){
                if(!isset($v)){
                    continue;
                }
                $i++;
                if($i > $num_max_el){
                    break;
                }
                if(($i % $lungh_riga) == 1){
                    $content.= "<li class='{$css_first}'>";
                }
                else if(($i % 3) == 0){
                    $content.= "<li class='{$css_last}'>";
                }
                else{
                    $content.= "<li class='{$css_normal}'>";
                }
                $content.= "<a href='index.php?id=".cercaPaginadaDescrizione('product')."&id_prod={$v['id']}'>";
                if($v['discount'] != 0){
                    $content.= "<span class='onsale'>Sale {$v['discount']}%</span> ";
                }
                $content.= "<img class='gallery' src='{$v['image']}' alt='' title='{$v['name']}' />";
                $content.="<div class='infoFilterContainer'>";
                $content.="<div class='single_star'>{$v['rating']}</div>";
                $content.="<h5 class='infoFilter'>{$v['impedence']}&#8486;</h5>";
                $content.="<h5 class='infoFilter'>{$v['sensitivity']}dB/mW</h5>";
                $content.="</div>";
                $content.= "<h3>{$v['name']}</h3>";
                $content.= "<span class='price'>&euro; {$v['price']}</span>";
                $content.= "</a></li>";
            }
            $content .="</ul>";
        }
        return $content;
    }
    
    function tendinaQuantita($name,$data,$pars){
       $content = "";
        $i;
        for($i = 0; $i < $data; $i++){
            $j = $i+1;
            $content .= "<option value='{$j}'>{$j}</option>";
        }
        return $content;
    }
    
    function visualizzaSpecs($name,$data,$pars){
        $content = "";
        if($data != NULL){
        $content = "<li><h3>Specifications:</h3></li>";
        $val = explode("|",$data);
        $content .="<li class='specs_list'>sensitivity: {$val[0]}</li>";
        $content .="<li class='specs_list'>impedence: {$val[1]}</li>";
        }
        return $content;
    }
    
    function visualizzaFeatures($name,$data,$pars){
        $content = "";
        if($data != NULL){
        $content = "<li><h3>Features:</h3></li>";
        $val = explode("|",$data);
        foreach($val as $v){
            if($v != NULL){
            $content .= "<li class='specs_list'>{$v}</li>";
            }
        }
        }
        return $content;
    }
    function visualizzaPercorsoProdotto($name,$data,$pars){
        $content = "<a class='home' href='index.php?id=".cercaPaginadaDescrizione('products')."'>Products</a>  &rsaquo; ";
        $query = "SELECT id,descr FROM 5_product_category,5_category WHERE id_product = {$data['id']} AND id_category = id ;";
        $cat = getResult($query);
        if(count($cat) > 1){
            if($cat[0]['descr'] == 'hi-end'){
                $content .= "<a class='home' href='index.php?id=".cercaPaginadaDescrizione($cat[1]['descr'])."'>{$cat[1]['descr']}</a> &rsaquo; ";
                $content .= "<a class='home' href='index.php?id=".cercaPaginadaDescrizione($cat[0]['descr'])."'>{$cat[0]['descr']}</a> &rsaquo; ";
            }
            else{
                $content .= "<a class='home' href='index.php?id=".cercaPaginadaDescrizione($cat[0]['descr'])."'>{$cat[0]['descr']}</a> &rsaquo; ";
                $content .= "<a class='home' href='index.php?id=".cercaPaginadaDescrizione($cat[1]['descr'])."'>{$cat[1]['descr']}</a> &rsaquo; ";
            }
            $content .= "<a class='home' href='index.php?id=".cercaPaginadaDescrizione('search')."&m={$data['brand']}&cat1={$cat[0]['id']}&cat2={$cat[1]['id']}'>{$data['brand']}</a>  &rsaquo; ";
        }
        else{
            $content .= "<a class='home' href='index.php?id=".cercaPaginadaDescrizione($cat[0]['descr'])."'>{$cat[0]['descr']}</a> &rsaquo; ";
            $content .= "<a class='home' href='index.php?id=".cercaPaginadaDescrizione('search')."&m={$data['brand']}&cat1={$cat[0]['id']}'>{$data['brand']}</a>  &rsaquo; ";   
        }
        $content .= " {$data['name']}";
        return $content;
    }
    
    function cssRating($name,$data,$pars){
        return "star_".$data;
    }
    
    function visualizzaCommenti($name,$data,$pars){
        $content = "";
        if(count($data) == 0){
            return $content;
        }
        foreach($data as $k => $v){
            $content .= "<article>{$v['data']}<div><div class={$pars['cssStar']}><span>{$v['rating']}</span></div><h1>{$v['title']}</h1></div><p>".nl2br($v['comment'])."</p></article>";
        }
        return $content;
    }
    
    function visualizzaFormCommenti($name, $data, $pars){
        if($data == ""){
            return "";
        }
        $par = explode(",",$data);
        $mex = "";
        if(isset($par[2])){
            $mex = "<p>".$par[2]."</p>"; 
        }
        return "<section class='t_prod_comment'>
    <header><h1>RATE THIS PRODUCT</h1></header>
    <section>
        <form action='index.php' method='get'>

            <input type='hidden' name='id' value='{$par[0]}'/>
            <input type='hidden' name='id_prod' value='{$par[1]}'/>
            <input name='title' type='text' maxlength='32' placeholder='TITLE - max 32 chars -' required='required'/>
            <textarea name='comment' maxlength='512' placeholder='REVIEW - max 512 chars -' required='required'></textarea>
            <div>
                <input class='button' type='submit' value='Submit Review'/>
                <p class='comment-form-rating'>
                    <select name='rating' id='rating' required='required'>
                        <option value=''>Rate this product&hellip;</option>
                        <option value='5'>Perfect</option>
                        <option value='4'>Good</option>
                        <option value='3'>Average</option>
                        <option value='2'>Not that bad</option>
                        <option value='1'>Very Poor</option>
                    </select>
                </p>
            </div>
        </form>
        {$mex}
    </section>
</section>";
    }
}
?>

