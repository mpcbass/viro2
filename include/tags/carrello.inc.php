<?php
require_once 'include/utilities.inc.php';
require_once 'include/dbms.inc.php';

Class carrello extends TagLibrary{
    function visualizzaTabella($name,$data,$pars){
        $content = "<tbody>";
        if($data==""){
            return "";
        }
        foreach($data as $k => $v){
            $content .= "<tr class='art_table_item'>";
            $content .= "<td class='product-remove'><span class='remove'>&times;</span></td>";
            $content .= "<td class='product-thumbnail'><a href='index.php?id=".cercaPaginadaDescrizione('product')."&id_prod={$v['id']}'><img src='{$v['image_small']}' alt='{$v['alt_text']}' title='' /></a></td>";
            $content .= "<td class='product-name'><a href='index.php?id=".cercaPaginadaDescrizione('product')."&id_prod={$v['id']}'>{$v['name']}</a></td>";
            $content .= "<td class='product-price'><span class='amount'>&euro;{$v['price']}</span></td>";
            $content .= "<td class='product-quantity'><div id='add_product_count'><select name='qta_prod{$v['id']}'>";
            for($j = 1; $j <= $v['qty']; $j++){
                if($j != $v['qty_ord']){
                    $content .= "<option value='{$j}'>{$j}</option>";
                }
                else{
                    $content .= "<option value='{$j}' selected='selected'>{$j}</option>";
                }
            }
            $content .= "</select></div></td>";
            $subtotal = round($v['price'] * $v['qty_ord'],2);
            if(!preg_match('/\.\d\d/', $subtotal)){
                if(!preg_match('/\.\d/',$subtotal)){
                    $subtotal .= ".00";
                }
                else{
                    $subtotal .= "0";
                }
            }
            $content .= "<td class='product-subtotal'><span class='amount'>&euro;".number_format($subtotal,2)."</span></td>";
            $content .= "</tr>";
        }
        $content .= "</tbody><tfoot><tr><td colspan='6' class='actions'>";
        $content .= "<input type='submit' class='button' name='update_cart' value='Update Cart' />";
        $content .= "<input type='submit' class='checkout-button button alt' name='proceed' value='Proceed to Checkout &rarr;' />";
        $content .= "</td></tr></tfoot>";
        return $content;
    }
    
    function visualizzaSubtotali($name,$data,$pars){
        if($data == NULL){
            $data = "0.00";
        }
        $content .= "<tr class='cart-subtotal'><th><strong>Cart Subtotal</strong></th><td><strong><span class='amount'>&euro;" .number_format($data,2)."</span></strong></td></tr>";
            $shipping_cost = 0.00;
        if(!preg_match('/\.\d\d/', $shipping_cost)){
                if(!preg_match('/\.\d/',$shipping_cost)){
                    $shipping_cost .= ".00";
                }
                else{
                    $shipping_cost .= "0";
                }
            }
        $content .= "<tr class='shipping'><th>Shipping</th><td>&euro; ". number_format($shipping_cost,2)."</td></tr>";
        $totale = round($shipping_cost + $data,2);
        if(!preg_match('/\.\d\d/', $totale)){
            if(!preg_match('/\.\d/',$totale)){
                $totale .= ".00";
            }
            else{
                $totale .= "0";
            }
        }
        $content .= "<tr class='total'><th><strong>Order Total</strong></th><td><strong><span class='amount'>&euro; ". number_format($totale,2)."</span></strong></td></tr>";
        return $content;
        
    }
}
?>
