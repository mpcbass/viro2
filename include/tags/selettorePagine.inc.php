<?php

class selettorePagine extends TagLibrary{
    function visualizzaBottoni($name, $data, $pars){
        if($data == ""){
            return "";
        }
        $max_num_pag_visualizzabili = $pars['max'];
        $start_el = $data['da'];
        $range = $pars['range'];
        #$last_el = $data['a'];
        $num_prod = $data['num_righe'];
        $num_pag = $num_prod / $range;
        #arrotondamento sempre per eccesso
        if(preg_match('/\d\.\d+/',$num_pag)){
            $num_pag = intval(preg_replace('/\.\d+/', "", $num_pag));
            $num_pag++;
        }
        
        $pag_attuale = $start_el / $range;#start_el è sempre un (multiplo di $range) + 1!
        $pag_attuale = intval(preg_replace('/\.\d+/',"",$pag_attuale));
        #inizio funzione
        if(isset($data['search'])){
            $p = cercaPaginadaDescrizione('search');
        }
        else{
            $p = cercaPaginadaDescrizione('product_list');
        }
        $content .="<nav class='prod_range'><div><input type='hidden' name='id' value='{$p}' />";
        unset($_GET['submit']);
        unset($_GET['el']);
        foreach($_GET as $k => $v){
           $content .= "<input type='hidden' name='{$k}' value='{$v}' />";
        }
        if(isset($data['search']) && $data['search'] != " "){
            $content .= "<input type='hidden' name='s' value='{$data['search']}' />";
        }
        else if(isset($data['brand']) && isset($data['cat1'])){
            $content .= "<input type='hidden' name='m' value='{$data['brand']}' />'";
            $content .= "<input type='hidden' name='cat1' value='{$data['cat1']}' />";
            if(isset($data['cat2'])){
                $content .= "<input type='hidden' name='cat2' value='{$data['cat2']}' />";
            }
        }
        $content .= "<button name='el' value ='0'>first</button>";#bottone inizio
        if($start_el >= $range){
            $start_el_page = $start_el - $range;
            $content .= "<button class='prod_page_prev' name='el' value='{$start_el_page}'>&laquo;</button>";
        }
        if($pag_attuale > 2){
            $start_selector = $pag_attuale-2;
        }
        else{
            $start_selector = 0;
        }
        $j = 0;
        for($i = $start_selector; $i < $num_pag; $i++){
            $j++;
            if($j == $max_num_pag_visualizzabili){
                break;
            }
            $start_el_page = $i * $range;
            if($i == $pag_attuale){
                $class_pag_att = "class='prod_page_selected'";
            }
            else{
                $class_pag_att = "";
            }
            $content .= "<button ".$class_pag_att." name='el' value='{$start_el_page}'>{$i}</button>";
        }
        if($start_el < $start_el_page){
            $start_el_page = $start_el + $range;
            $content .= "<button class='prod_page_next' name='el' value ='{$start_el_page}'>&raquo;</button>";
        }
        $num_pag--;
        $start_el_page = $num_pag * $range;
        $content .= "<button name='el' value='{$start_el_page}'>last</button>";
        $content .= "</div></nav>";
        return $content;
    }
}
?>
