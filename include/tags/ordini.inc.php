<?php
require_once 'include/dbms.inc.php';
require_once 'include/utilities.inc.php';

Class ordini extends TagLibrary {

    function visualizzaOrdini($name, $data, $pars) {
        $content = "";
        $ord_prec = "";
        $i = 0;
        foreach ($data as $k => $v) {
            if ($v['n_ord'] != $ord_prec) {
                if($i != 0){
                    $content .= "</tbody>";
                }
                $content .= "<div class='inner-container'>							
    <table class='shop_table cart resumeOrder' cellspacing='0'>
        <thead>
        <tr class='orderHead'>
            <td>
                <p class='orderNum' >Order N° {$v['n_ord']}</p>
                <p>Date: {$v['date']}</p>
            </td>
            <td colspan='3'>
                <p>Address: {$v['delivery_address']}</p>
                <p>City: {$v['delivery_city']} Country: {$v['delivery_country']}</p>
            </td>
            <td>
                <p>Total Order</p>
                <p>{$v['total']}&euro;</p></td>
            </tr>
            <tr>
                "./*<th class='product-remove'>&nbsp;</th>*/"
                <th class='product-thumbnail'></th>
                <th class='product-name'>Product</th>
                <th class='product-price'>Price</th>
                <th class='product-quantity'>Quantity</th>
                <th class='product-subtotal'>Subtotal</th>
            </tr>
        </thead><tbody>";
            }
            $query = "SELECT * FROM 5_product WHERE id = {$v['id_prod']}";
            $product = getResult($query);
            $content .= "<tr class='art_table_item'>";/*<td class='product-remove'><span class='remove'>&times;</span>*/
            $content .= "<td class='product-thumbnail'><a href='index.php?id=".cercaPaginadaDescrizione('product')."&id_prod={$product[0]['id']}'><img src='{$product[0]['image_small']}' alt='{$product[0]['alt_text']}' title='' /></a></td>";
            $content .= "<td class='product-name'><a href='index.php?id=".cercaPaginadaDescrizione('product')."&id_prod={$product[0]['id']}'>{$product[0]['name']}</a></td>";
            $content .= "<td class='product-price'><span class='amount'>&euro;{$product[0]['price']}</span></td>";
            $content .= "<td class='product-quantity'>{$v['qty']}";
            $content .= "<td class='product-subtotal'><span class='amount'>&euro;{$v['total_prod']}</span></td>";
            $content .= "</tr>";
            $ord_prec = $v['n_ord'];
            $i++;
        }
        return $content."</table></div>";
    }

}

?>
