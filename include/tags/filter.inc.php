<?php

require_once 'include/utilities.inc.php';

class filter extends TagLibrary {

    function visualizzaFiltriBase($name, $data, $pars) {
        $content = "<ul>";
        $i = 0;
        foreach ($data as $k => $v) {
            $i++;
            $content .= "<li><input type='checkbox' ";

            if (isset($pars['valueEnabled']) && $pars['valueEnabled'] == '1' && isset($pars['filterBaseName'])) {
                if (isset($_GET[$pars['filterBaseName'].$v['id']])) {
                    $content .= "checked = 'checked' ";
                }
                $content.="name='{$pars['filterBaseName']}{$v['id']}' value='{$v['value']}' />
                <label>{$v['descr']}</label></li>";
            } else {
                if (isset($_GET["id_category".$v['id']])) {
                    $content .= "checked = 'checked' ";
                }
                $content.="value='id_category={$v['id']}' name='id_category{$v['id']}' />
                <label>{$v['descr']}</label></li>";
            }
        }
        $content.="</ul>";

        return $content;
    }

    function visualizzaRating($name, $data, $pars) {
        $ids = explode(",", $pars['idcss']);
        $content = "<ul>";
        $i;
        for ($i = 0; $i <= 5; $i++) {
            $checked = "";
            if(isset($_GET["rating".$i])){
                $checked = "checked='checked'";
            }
            $content .= "<li class='{$pars['classcss']}'><span><input id='{$ids[$i]}' name = 'rating{$i}' {$checked} type='checkbox' value='rating={$i}' /></span></li>";
        }
        $content.="</ul>";
        return $content;
    }

}

?>
