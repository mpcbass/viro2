<?php

DEFINE("NO_GENITORE", 0);

Class menu extends TagLibrary {

    var $menu_stampati,
            $array_figli_padri,
            $array_padri_figli,
            $array_entry,
            $fine_menu,
            $links,
            $principale,
            $content;

    #funzione ausiliaria che ritorna il valore del primo id_menu che non è stato stampato(ricordare che i menu sono stati già ordianti!)

    function finito() {
        foreach ($this->menu_stampati as $k => $v) {
            if (!$v) {
                return $k;
            }
        }
        return -1;
    }

    function riempi_contenuto_header($k, $flag, $set, $class_sub) {
        if ($k < 0) {
            return $this->content;
        }
        $c = 0;
        while (!$c) {
            if (!isset($this->array_padri_figli[$k][0]) || $this->array_entry[$k] == "Home") {

                if (!$this->menu_stampati[$k]) {
                    $noul = 0;
                    $i = 0;
                    $this->content.="<li";
                    #bisogna stampare la class current_menu_item solo per la prima voce del menu
                    #+bisogna stampare la class last-menu-item solo per l'ultima voce del menu
                    if ($set == 0 && $this->fine_menu != $k) {
                        $this->content .= " class='current_menu_item'";
                        $set = 1;
                    } else if ($set < 2 && $this->fine_menu == $k) {
                        $this->content.=" class='last-menu-item'";
                        $set = 2;
                    }
                    $this->content .= "><a href='{$this->links[$k]}'>" . $this->array_entry[$k] . "</a>";
                    $this->menu_stampati[$k] = 1;
                }
                #stampare i figli del menu
                foreach ($this->array_padri_figli[$k] as $key => $value) {
                    $i++;
                    if ($key == 0) {#caso del menu principale senza sottomenu
                        $noul = 1;
                        break;
                    }
                    if ($i == 1) {
                        $this->content.="<ul class='{$class_sub}'>";
                    }
                    $this->riempi_contenuto_header($key, 1, $set, $class_sub);
                }
                if ($noul == 1) {
                    $this->content .= "</li>";
                } else {
                    $this->content .= "</ul></li>";
                }
                $c++;
            } else {
                $this->content.="<li><a href='{$this->links[$k]}'>" . $this->array_entry[$k] . "</a></li>";
                $this->menu_stampati[$k] = 1;
                $c++;
            }
        }
        if ($flag) {
            return;
        }
        $this->riempi_contenuto_header($this->finito($this->menu_stampati), 0, $set, $class_sub);
    }

    function riempi_contenuto($k, $flag, $set, $class_sub) {
        if ($k < 0) {
            return $this->content;
        }
        $c = 0;
        while (!$c) {
            if (isset($this->array_padri_figli[$k][0]) && !$this->menu_stampati[$k]) {
                $this->content.="<li"; /*
                  if($set == 0 && !isset($this->fine_menu[$k])){
                  $this->content .= " class='current_menu_item'";
                  $set = 1;
                  }
                  else if($set < 2 && isset($this->fine_menu[$k])){
                  $this->content.=" class='last-menu-item'";
                  $set = 2;
                  } */
                $this->content.="><a href='{$this->links[$k]}'>" . $this->array_entry[$k] . "</a></li>";
                $c++;
                $this->menu_stampati[$k] = 1;
            } else {
                $this->content.="<li><a href='{$this->links[$k]}'>" . $this->array_entry[$k] . "</a>";
                $this->content.="<ul class='{$class_sub}'>";
                $this->menu_stampati[$k] = 1;
                foreach ($this->array_padri_figli[$k] as $key => $value) {
                    $this->riempi_contenuto($key, 1, $set, $class_sub);
                }
                $this->content.="</ul></li>";
                $c++;
            }
        }
        if ($flag) {
            return;
        }
        $this->riempi_contenuto($this->finito($this->menu_stampati), 0, $set, $class_sub);
    }

    #funzione che inizializza le variabili

    function visualizza_menu($name, $data, $pars) {
        $this->content = "";
        foreach ($data as $k => $v) {
            $current_padre = $v['id'];
            $current_figlio = $v['id_figlio'];
            $genitore = $v['id_parent'];
            $posizione_padre = $v['posizione'];
            $posizione_figlio = $v['pos_figlio'];
            $valore = $v['value'];
            $ultimo = $v['last'];

            if ($valore == "Home") {
                $this->principale[$current_padre] = 1;
            }
            if ($current_figlio != NULL) {
                $this->array_padri_figli[$current_padre][$current_figlio] = 1;
            } else {
                $this->array_padri_figli[$current_padre][0] = 1;
            }
            if ($ultimo == 1) {
                $this->fine_menu = $current_padre;
            }
            $this->menu_stampati[$current_padre] = 0;
            $this->array_entry[$current_padre] = $valore;
            $this->links[$current_padre] = $v['link_path'];
            $this->array_figli_padri[$current_padre] = $genitore;
        }
        if ($pars['tipo'] == 'header') {
            $this->riempi_contenuto_header($this->finito($this->menu_stampati), 0, 0, $pars['submenuclass']);
        } else {
            $this->riempi_contenuto($this->finito($this->menu_stampati), 0, 0, $pars['submenuclass']);
        }
        return $this->content;
    }

}

?>
