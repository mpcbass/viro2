<?php

require_once 'include/dbms.inc.php';
require_once 'include/utilities.inc.php';

define('DEFAULT_USER_PREFIX', 't_'); // user_type t_user utenti temporanei
define('DEFAULT_USER_GROUP','3');   // user_group utenti temporanei oppure non ancora loggati
define('LAST_ACTIVITY_TIME_LIMIT',60*60*24*7); // 7 giorni

class AccessMng
{ 
   /*
    *    query DB
    *    Settare/verificare $_SESSION
    *     
    */
   function AccessMng()
   { 
       /*
        *   controllo di validità della sessione
        */
      
       if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > LAST_ACTIVITY_TIME_LIMIT )) 
       {
            session_unset();     // unset $_SESSION variable for the run-time 
            session_destroy();   // destroy session data in storage
            session_start();
        }
        $_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
       
        /*
         *  A questo punto siamo certi di avere una sessione valida
         */
        
        if( !isset($_SESSION['username']) ) 
        {
            /* 
             *    non è settato username, quindi si assume che l'utente sia t_user
             */
            $_SESSION['username']=DEFAULT_USER_PREFIX.session_id();
            $_SESSION['user_group']=DEFAULT_USER_GROUP;
            $_SESSION['logged'] = 0;

            /*
             *    precaricaricare i servizi ai quali si hanno accesso dentro $_SESSION 
             */
            $query="SELECT id,file_name FROM 1_service_group JOIN 1_service ON id=id_service AND id_group={$_SESSION['user_group']}";
            $res = getResult($query);
            foreach( $res as $key=>$value)
            {
               #echo $value['id']."_____".$value['file_name'];
               $_SESSION['page'][$value['id']]=$value['file_name'];
            }
         } 
      }

      function accessPage($page_id)
      {
         foreach ($_SESSION['page'] as $key => $value) 
         {
            if($key==$page_id)
               return true;
         }
         return false;
      }
}

#cambia lo username della sessione insieme a tutti gli script in base al gruppo di appartenenza
function cambiaSettaggiSession($username)
{
    $_SESSION['username'] = $username;
    $_SESSION['logged'] = secure_logged($_SESSION['username']);
    unset($_SESSION['page']);
    $query = "SELECT id_group FROM 1_user JOIN  1_user_group ON id = id_user WHERE username = '{$username}';";
    $ris = getResult($query);
    $_SESSION['user_group'] = $ris[0]['id_group'];
    
    $query="SELECT id,file_name FROM 1_service_group JOIN 1_service ON id=id_service AND id_group={$_SESSION['user_group']};";
    $ris = getResult($query);
    foreach( $ris as $key=>$value)
    {
        $_SESSION['page'][$value['id']] = $value['file_name'];
    }
    ricaricaCarrello();
    return 1;
}

#ritorna l'identificativo dell'utente che si sta loggando altrimenti ritorna 0
function verificaCredenziali($username,$password){
    $query = "SELECT id,username FROM 1_user WHERE username='{$username}' AND password='".cripta_password($password, $username)."';";
    $ris = getResult($query);
    if($ris)
        return $ris[0]['id'];
    else
        return $ris;#cioè 0 (vedi getresult -> dbms.inc.php)
}

?>
