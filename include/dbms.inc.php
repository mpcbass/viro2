<?php
DEFINE("QUERY_MULTIPLA",-2);
DEFINE("CONN_DB_NAME",1001);
DEFINE("CONN_DBMS",1002);
DEFINE("CONN_OK",0);

Class Connection{

	var $host,
	$user,
	$pass,
	$db,
	$active = false,
	$errno = 0;

	function Connection($host, $user, $pass, $db) {
			
		$this->host = $host;
		$this->user = $user;
		$this->pass = $pass;
		$this->db = $db;
	}

	function connect() {
			
		if (!$this->active) {

			$conn = mysql_pconnect($this->host, $this->user, $this->pass);

			if ($conn) {
				$sel = mysql_select_db($this->db);

				if ($sel) {

					return true;

				} else {

					$this->errno = CONN_DB_NAME;
					return false;
				}
			} else {
					
				$this->errno = CONN_DBMS;
				return false;
					
			}
		}
	}

	function error() {
		switch ($this->errno) {
			case CONN_DB_NAME:
				echo "nome errato!";
				break;
			case CONN_DBMS:
				echo "connessione errata!";
				break;
			case CONN_OK:
				$this->active = true;
				echo "tuuuuto ok!";
				break;
					
			default:
				echo "errore sconosciuto!";
				break;
		}
	}
}

$mydb = new Connection("localhost",'teo2', 'teo2', 'mydb');
$mydb->connect();

function getResult($query) {
	if(!preg_match('/;+\w+;+/', $query))
        {
            $oid = mysql_query($query);
            if($oid == 0){
                echo mysql_error();
                return 0;
            }
            if(mysql_num_rows($oid) == 0){
                return 0;
            }
            while($data = mysql_fetch_assoc($oid))
            {
               $content[] = $data;
            }
            mysql_free_result($oid);
            return $content;
	}
	return QUERY_MULTIPLA;
}

function queryInsert($query){
    if(!preg_match('/;+\w+;+/', $query)){
            $oid = mysql_query($query);
            if($oid == 0){
                echo mysql_error();
                return 0;
            }
            if(mysql_affected_rows($oid)== -1){
                return 0;
            }
            else{
                return 1;
            }
	}
	return QUERY_MULTIPLA;
}

function avoid_sql_injection($array){
    foreach ($array as $k => $v){
        mysql_set_charset('utf8_unicode_ci');
        $array[$k] = mysql_real_escape_string($v);
    }
    return $array;
}
#echo $mydb->error();
?>
