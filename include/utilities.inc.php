<?php

require_once 'include/dbms.inc.php';

function cripta_password($password,$username)
{
    $pwd_criptata = sha1($username.$password);
    return $pwd_criptata;
}

function secure_logged($user)
{
    $key = "chiaveincomprensibilissima!";
    $crypt = sha1($user.$key);
    return $crypt;
}

function isLogged()
{
    if($_SESSION['logged'] === secure_logged($_SESSION['username'])){
        return 1;
    }
    return 0;
}

function isAdmin()
{
    $query = "SELECT id FROM 1_group WHERE descr = 'admin';";
    $ris = getResult($query);
    
    if($_SESSION['user_group'] == $ris[0]['id']){
        return 1;
    }
    return 0;
}

function isTempUser()
{
    $pattern = '/t_.+/';
    if(preg_match($pattern, $_SESSION['username'])){
            return 1;
    }
    return 0;
}

function isRegUser()
{
    $query = "SELECT id FROM 1_group WHERE descr = 'p_user';";
    $ris = getResult($query);
    
    if($_SESSION['user_group'] == $ris[0]['id']){
        return 1;
    }
    return 0;
}

function menu($descr)
{
    $query = "SELECT id FROM 2_menu WHERE descr = '{$descr}';";
    $ris = getResult($query);
    if(!isset($ris)){
        return 0;
    }
    $query = "
    SELECT 2_menu_field . * , 2_menu_composition.last AS last, 2_menu_field_bis.id AS id_figlio, 2_menu_field_bis.value AS entry_figlio, 2_menu_field_bis.posizione AS pos_figlio
    FROM (
    2_menu_field
    JOIN 2_menu_composition ON 2_menu_field.id = 2_menu_composition.id_field
    AND 2_menu_composition.id_menu = {$ris[0]['id']}
    )
    LEFT JOIN 2_menu_field AS 2_menu_field_bis ON 2_menu_field.id = 2_menu_field_bis.id_parent
    ORDER BY 2_menu_field.id_parent, 2_menu_field.posizione, 2_menu_field_bis.posizione;
    ";
    return getResult($query);
}

function slides($pagina)
{
    $query = "
        SELECT * 
        FROM 6_slide WHERE id_page = {$pagina}
        ORDER BY posizione;
        ";
    return getResult($query);
}

function select_categorie($cats){
    $result = "";
    foreach($cats as $k => $v){
        $result .= "<option value='{$v['id']}'>".$v['descr']."</option>";
    }
    return $result;
}

#funzione che ritorna un array di 3 elementi. il primo contiene il num dei prodotti nel carrello
#+ il secondo il costo totale del carrello attuale e il terzo contiene il carattere aggiuntivo "s" per dare eventualmente iil prulare alla parola item
#+ se il carrello non è stato istanziato allora ritorna NULL
function carrello()
{
    $ris = array(0,0,"");
    if(isset($_SESSION['cart']['prods']) && count($_SESSION['cart']['prods']) > 0){
        foreach($_SESSION['cart']['prods'] as $k => $v){
            $ris[1] += $v;
        }
        if($ris[1] > 1){
            $ris[2] = "s";
        }
        $ris[0] = $_SESSION['cart']['tot'];
        return $ris;
    }
    return NULL;
}

$utente = array();
function salvaCarrello(){
    global $utente;
    if(isLogged() && isset($_SESSION['cart'])){
        if (!preg_match('/\.\d\d/', $_SESSION['cart']['tot'])) {
                if (!preg_match('/\.\d/', $_SESSION['cart']['tot'])) {
                    $_SESSION['cart']['tot'] .= ".00";
                } else {
                    $_SESSION['cart']['tot'] .= "0";
                }
        }
        $query = "SELECT id FROM 1_user WHERE username = '{$_SESSION['username']}';";
        $utente = getResult($query);
        $prodotti = "";
        foreach($_SESSION['cart']['prods'] as $k => $v){
            /*if (!preg_match('/\.\d\d/', $_SESSION['cart']['prods'][$k])) {
                if (!preg_match('/\.\d/', $_SESSION['cart']['prods'][$k])) {
                    $_SESSION['cart']['prods'][$k] .= ".00";
                } else {
                    $_SESSION['cart']['prods'][$k] .= "0";
                }
            }*/
            $prodotti .= "|".$k."=".$v; 
        }
        if($_SESSION['cart']['saved'] == 1){
            $query = "UPDATE 5_cart SET detail = '{$prodotti}', date = CURDATE(), tot = {$_SESSION['cart']['tot']} WHERE id_user = {$utente[0]['id']};";
        }
        else{
            $query = "INSERT INTO 5_cart VALUES('',{$utente[0]['id']},'{$prodotti}',CURDATE(),{$_SESSION['cart']['tot']});";
            $_SESSION['cart']['saved'] = 1;
        }
        queryInsert($query);
    }
    return 1;
}

#funzione che ricarica nella sessione il contenuto del carrello per un utente appena loggato
function ricaricaCarrello(){
    $query = "SELECT * FROM 5_cart WHERE id_user = (SELECT id FROM 1_user WHERE username = '{$_SESSION['username']}');";
    $ris = getResult($query);
    if($ris != 0){
        $_SESSION['cart']['tot'] += $ris[0]['tot'];
        $prodotti = explode("|", $ris[0]['detail']);
        #elimino l'ultimo elemento di prodotti poichè è nullo!
        unset($prodotti[0/*count($prodotti)-1*/]);
        foreach($prodotti as $k => $v){
            if($k == 0){
                continue;
            }
            preg_match('/(.+)\=(.+)/', $v, $matches);
            #se il carrello temporaneo contiene un prodotto che era presente nel carrello esistente dell'utente
            #+ le informazioni relative a questo prodotto saranno sovrascritte da quelle presenti nel carrello attuale
            if(isset($_SESSION['cart']['prods'][$matches[1]])){
                $query = "SELECT price FROM 5_product WHERE id = {$matches[1]};";
                $ris = getResult($query);
                $_SESSION['cart']['tot'] -= $ris[0]['price'] * $matches[2];
            }
            else{
                $_SESSION['cart']['prods'][$matches[1]] = $matches[2];
            }
        }
        $_SESSION['cart']['saved'] = 1;
    }
    salvaCarrello();
    return 1;
}

function categorie(){
    $query = "SELECT * FROM 5_category ORDER BY(descr);";
    return getResult($query);
}

#restituisce l'id della pagina richiesta
function cercaPaginadaDescrizione($descr){
    $query = "SELECT id FROM 1_service WHERE file_name = '{$descr}.php'";
    $result = getResult($query);
    return $result[0]['id'];
}

function rangePrezzo(){
    $query = "SELECT * FROM 5_rangePrezzo ORDER BY(id);";
    return getResult($query);
}
function rangeSconto(){
    $query = "SELECT * FROM 5_rangeSconto ORDER BY(id);";
    return getResult($query);
}
?>
