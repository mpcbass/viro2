<?php

require_once 'include/dbms.inc.php';

class filtering{
    var $filter_applied, $cat;
    
    function filtering(){
        if(isset($_SESSION['filter'])){
            foreach($_SESSION['filter'] as $k => $v){
                $this->filter_applied[$k] = $v;
            }
        }
        else{
            $this->filter_applied = array();
        }
        #set categories
        $query = "SELECT * FROM 5_category ORDER BY(descr);";
        $result = getResult($query);
        foreach($result as $k => $v){
            $this->cat[] = $v;
        }
    }
    
    function filtra($delete, $set){
        $array_campi = array();
        $cat = array();
        $query_cat = "";
        
        foreach ($delete as $k => $v){
            unset($this->filter_applied[$k]);
        }
        foreach($set as $k => $v){
            $this->filter_applied[$k] = preg_replace("/\'/", "", $v);
        }
        foreach($this->filter_applied as $k => $v){
            if(preg_match("/^id_category\d/",$k)){#gestione diverse per le categorie poichè sono in un altra tabella
                $cat[] = $v;
                continue;
            }
            #DEBUG
            
            if(preg_match("/netbeans/",$v)){
                unset($this->filter_applied[$k]);
                continue;
            }
            $array_campi[$k] = $v;
        }
        $order_by = "";
        
        #IMPOSTAZIONE QUERY IN CASO FOSSERO SPECIFICATE LE CATEGORIE DEI PRODOTTI
        if(count($cat) > 0){
            $where_cat = implode(' OR ', $cat);
        }
        else{
            $where_cat = 1;
        }
        #IMPOSTAZIONE QUERY NEL CASO IN CUI SIANO APPLICATI FILTRI DIVERSI DALLE CATEGORIE
        if(count($array_campi) > 0){
            ksort($array_campi,SORT_NATURAL);
            $query_cat = " AND ";
            $order_by = " ORDER BY";
            foreach($array_campi as $k => $v){
                $k = preg_replace('/(.+)(\d)/',"\$1",$k);
                if(! preg_match("/$k/",$order_by)){
                    $order_by .= " $k,";
                }
            }
            $array_campi_raggr = array();
            $raggruppato = array();
            foreach($array_campi as $k => $v){
                if(!$raggruppato[$k]){
                    $campo = preg_replace('/(.+)(\d)/',"\$1",$k);
                    $array_campi_raggr[$campo] = "( $v";
                    foreach($array_campi as $key => $value){
                        if($k != $key){
                            if(preg_match("/$campo(\d)/",$key)){
                                $array_campi_raggr[$campo] .= " OR ".$value;
                                $raggruppato[$key] = 1;
                            }
                        }
                    }
                    $array_campi_raggr[$campo] .=" ) ";
                }
            }
            #pulizia
            foreach($array_campi_raggr as $k => $v){
                $array_campi_raggr[$k] = preg_replace('/(OR)  (\))/',"\$2",$v);
            }
        }
        #NEL CASO FOSSERO SPECIFICATE PIÙ CATEGORIE È NECESSARIO MODIFICARE LA QUERY
        if(count($cat) > 1){
            $query_cat .= "id IN (SELECT id_product FROM 5_product_category WHERE ".$where_cat." GROUP BY(id_product) HAVING COUNT(*) = ".count($cat).")";
        }
        else{
            $query_cat .= "id IN (SELECT id_product FROM 5_product_category WHERE ".$where_cat.")";
        }
        $where = implode(' AND ', $array_campi_raggr);
        $where = preg_replace('/([a-zA-Z\_]+)(\d)/',"\$1",$where);
        $query = "SELECT * FROM 5_product WHERE ".$where.$query_cat.$order_by;
        $query = preg_replace('/,$/', "", $query);
        return getResult($query.";");
    }
    
    function getFilter(){
        foreach($this->filter_applied as $k => $v){
            $array[] = $k;
        }
        return $array;
    }
    
    function getCat(){
        return $this->cat;
    }
    function salva(){
        $_SESSION['filter'] = $this->filter_applied;
    }
}
?>
