<?php

require_once 'include/template2.inc.php';
require_once 'include/dbms.inc.php';
require_once 'include/utilities.inc.php';

if (isset($_GET['el'])) {
    $da = $_GET['el'];
} else {
    $da = 0;
}
$info_page_selector = array();

function filtra($param) {
    $n_cat = 0;
    $n_price = 0;
    $n_rating = 0;
    $n_discount = 0;
    $n_sens = 0;
    $n_imp = 0;
    $query = "SELECT * FROM 5_product WHERE ";
    $query_cat = "id IN (SELECT id_product FROM 5_product_category WHERE ";
    $query_price = "";
    $query_rating = "";
    $query_discount = "";
    $query_sens = "";
    $query_imp = "";
    $order_by = "";
    foreach ($param as $k => $v) {
        if (preg_match('/cat/', $k)) {
            $n_cat++;
            if ($n_cat == 1) {
                $query_cat .= $v;
            } else {
                $query_cat .= " OR " . $v;
            }
        } else if (preg_match('/price/', $k)) {
            $n_price++;
            if ($n_price > 1) {
                $query_price .= " OR ";
            } else {
                $order_by .= " price";
            }
            $query_price .= $v;
        } else if (preg_match('/rating/', $k)) {
            $n_rating++;
            if ($n_rating > 1) {
                $query_rating .= " OR ";
            } else {
                $order_by .= " rating";
            }
            $query_rating .= $v;
        } else if (preg_match('/discount/', $k)) {
            $n_discount++;
            if ($n_discount > 1) {
                $query_discount .= " OR ";
            } else {
                $order_by .= " discount";
            }
            $query_discount .= $v;
        } else if (preg_match('/impedence/', $k)) {
            $n_imp++;
            if ($n_imp > 1) {
                $query_imp .= " OR ";
            } else {
                $order_by .= " discount";
            }
            $query_imp .= $v;
        } else if (preg_match('/sensitivity/', $k)) {
            $n_sens++;
            if ($n_sens > 1) {
                $query_sens .= " OR ";
            } else {
                $order_by .= " sensitivity";
            }
            $query_sens .= $v;
        }
    }
    if ($n_cat > 1) {
        $query_cat .= " GROUP BY(id_product) HAVING COUNT(*) = {$n_cat}";
    }
    if ($n_cat == 0) {
        $query_cat .= "1";
    }
    $query_cat .= ")";
    if ($order_by != "") {
        $order_by = preg_replace('/(\ )/', "", $order_by, 1);
        $order_by = preg_replace('/\ /', ",", $order_by);
        $order_by = "ORDER BY " . $order_by;
    }
    if ($query_price != "") {
        $query_price = "(" . $query_price . ") AND ";
    }
    if ($query_discount != "") {
        $query_discount = "(" . $query_discount . ") AND ";
    }
    if ($query_imp != "") {
        $query_imp = "(" . $query_imp . ") AND ";
    }
    if ($query_sens != "") {
        $query_sens = "(" . $query_sens . ") AND ";
    }
    if ($query_rating != "") {
        $query_rating = "(" . $query_rating . ") AND ";
    }
    $query .= $query_price . $query_discount . $query_imp . $query_sens . $query_rating . $query_cat . $order_by . ";";
    return getResult($query);
}

function search($param) {
    global $info_page_selector;
    if (isset($param['m']) && isset($param['cat1'])) {
        $info_page_selector['range'] = 9; #forse fonte di problemi
        $info_page_selector['search'] = " ";
        $info_page_selector['cat1'] = $param['cat1'];
        $param['m'] = preg_replace('/\+/', " ", $param['m']);
        $info_page_selector['brand'] = $param['m'];
        $query = "SELECT * FROM 5_product WHERE brand = '{$param['m']}' AND id IN 
    (SELECT id_product FROM 5_product_category WHERE id_category = {$param['cat1']}";
        if (isset($param['cat2'])) {
            $info_page_selector['cat2'] = $param['cat2'];
            $query .= " OR id_category = {$param['cat2']} GROUP BY(id_product) HAVING COUNT(*) = 2 ";
        }
        $query .= ");";
    } else {
        $param['s'] = preg_replace('/\+/', " ", $param['s']);
        $query = "SELECT * FROM 5_product WHERE name LIKE '%{$param['s']}%' OR brand LIKE '%{$param['s']}%' OR id IN (
        SELECT id_product FROM 5_category,5_product_category WHERE descr LIKE '%{$param['s']}%' 
        AND 5_category.id = 5_product_category.id_category
        );";
    }
    return getResult($query);
}

function cat_phones($param) {
    $query = "SELECT * FROM 5_product WHERE id IN (
        SELECT id_product FROM 5_category,5_product_category WHERE descr LIKE '%" . preg_replace('/(.+)(\..+)/', "\$1", $param) . "%' 
        AND 5_category.id = 5_product_category.id_category
        ) ORDER BY rating DESC,votes DESC;";
    return getResult($query);
}

$griglia = new Template('skin/dtml/b_product_grid_search.html');
$bool = 0;
if (isset($_GET['s']) || isset($_GET['m'])) {
    $bool = 1;
    $res = search($_GET);
    $griglia->setContent('titolo', 'Results');
} else {
    if (!isset($_GET['id'])) {
        $_GET['id'] = cercaPaginaDaDescrizione('home');
    }
    $query = "SELECT file_name FROM 1_service WHERE id = {$_GET['id']};";
    $cat = getResult($query);
    if (!isset($_GET['filter'])) {
        if (preg_match('/home/', $cat[0]['file_name'])) {
            $bool = 1;
            $griglia->setContent('titolo', 'Most Rated');
            $res = cat_phones("");
        } else if (preg_match('/product_list.php/', $cat[0]['file_name'])||preg_match('/search.php/', $cat[0]['file_name'])) {
            $bool = 1;
            $griglia->setContent('titolo', 'Products List');
            $res = getResult("SELECT * FROM 5_product ORDER BY name;");
        } else {
            $bool = 1;
            $griglia->setContent('titolo', "Most Rated " . preg_replace('/(.+)(\..+)/', "\$1", $cat[0]['file_name']));
            $res = cat_phones($cat[0]['file_name']);
        }
    }
}
if (!$bool) {
    $res = filtra($_GET);
    $griglia->setContent('titolo', 'Filtered Products');
}

$info_page_selector['num_righe'] = count($res);
$info_page_selector['da'] = $da;
$griglia->setContent('selettorePagine', $info_page_selector);
$elementi_in_pagina = array();
for ($i = $da; $i < count($res); $i++) {
    $elementi_in_pagina[] = $res[$i];
}
if ($res == 0) {
    $elementi_in_pagina = 0;
}
$griglia->setContent('prodotti', $elementi_in_pagina);
echo $griglia->get();
?>
