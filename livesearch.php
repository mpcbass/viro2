<?php

require_once "include/dbms.inc.php";
/*
  $query = "SELECT name FROM 5_product WHERE name LIKE '%{$_GET['s']}%';";
  $ris = getResult($query);
  $hint = "";
  foreach($ris as $k => $v){
  $hint .= $v['name']."<br />";
  }

  if ($hint=="")
  {
  $response="no suggestion";
  }
  else
  {
  $response=$hint;
  }
  echo $response;
 */
if (isset($_GET['s']) && $_GET['s']!="" ) {
    
    $aux=str_split($_GET['s']);
    $regex[]="%";
    foreach($aux as $i)
       $regex[]=$i."%";
    $regex=implode("",$regex);
    
    $response = [];
    $query[] = "SELECT name as str FROM 5_product WHERE name LIKE '{$regex}' LIMIT 0,10;";
    $query[] = "SELECT descr as str FROM 5_category WHERE descr LIKE '%{$regex}%' LIMIT 0,10;";
    $query[] = "SELECT brand as str FROM 5_product WHERE brand LIKE '%{$regex}'GROUP BY brand LIMIT 0,10";

    foreach ($query as $x) {
        $ris = getResult($x);
        foreach ($ris as $k => $v) {
            $response[] = $v['str'];
        }
    }
    
    echo json_encode($response);
} 
else
    echo json_encode(array());

?>
