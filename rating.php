<?php
require_once 'include/dbms.inc.php';
require_once 'include/utilities.inc.php';

if($_GET['title'] != "" && $_GET['comment'] != "" && $_GET['rating'] !=""){
    $paginaProd = cercaPaginadaDescrizione('product');
    $valori = avoid_sql_injection($_GET);
    $valori['title'] =  strip_tags($valori['title']);
    $valori['comment'] = htmlspecialchars($valori['comment']);
    $utente = getResult("SELECT id FROM 1_user WHERE username = '{$_SESSION['username']}';");
    $query = "INSERT INTO 5_rating VALUES('','{$valori['title']}','{$valori['comment']}',{$_GET['id_prod']},{$utente[0]['id']},{$valori['rating']},CURDATE());";
    if(queryInsert($query) === 1){
        header("location:index.php?id={$paginaProd}&id_prod={$_GET['id_prod']}");
    }
    else{
        header("location:index.php?id={$paginaProd}&e_mex=You've already expressed your opinion on this product!&id_prod={$_GET['id_prod']}");
    }
}
else{
    header('location:error.php?e_type=rating');
}

?>
