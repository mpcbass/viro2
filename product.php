<?php

session_start();

require_once "include/dbms.inc.php";
require_once "include/template2.inc.php";
require_once "include/utilities.inc.php";

if(isset($_GET['id'])){
    $id_pagina = $_GET['id'];
}
else{
    $id_pagina = cercaPaginadaDescrizione('product');
}
unset($_GET['id']);
$e_mex = "";
if(isset($_GET['e_mex'])){
    $e_mex = ",".$_GET['e_mex'];
}
##################
#dtml principale
$main = new Template('skin/dtml/t_frame-public.html');
##################

#########
#carrello
$carrello = carrello();
if($carrello != NULL){
    $top_cart = new Template('skin/dtml/b_cart.html');
    $top_cart->setContent('price_amount', number_format($carrello[0],2));
    $top_cart->setContent('item_num', $carrello[1]);
    $top_cart->setContent('item_plural', $carrello[2]);
    $top_cart->setContent('id_cart', cercaPaginadaDescrizione('cart'));
    $main->setContent('cart', $top_cart->get());
}else{$top_cart = new Template('skin/dtml/b_cart.html');
    $top_cart->setContent('price_amount', '0.00');
    $top_cart->setContent('item_num', '0');
    $top_cart->setContent('item_plural', "");
    $top_cart->setContent('id_cart', cercaPaginadaDescrizione('cart'));
    $main->setContent('cart', $top_cart->get());
}
#########

####################################################
#istanziazione degli oggetti necessari per la pagina
$nav_bar = new Template('skin/dtml/b_main_nav_bar.html');
$search_bar = new Template('skin/dtml/b_search_bar.html');
$flt = new Template('skin/dtml/b_side_filters3.html');
$prodotto = new Template('skin/dtml/b_product_view_2.html');


$query = "SELECT * FROM 5_product WHERE id = {$_GET['id_prod']};";
$ris = getResult($query);

################################
#saluto utente oppure side login
if(isLogged()){
    $user_greetings=new Template('skin/dtml/b_side_user_greetings.html');
    $user_greetings->setContent('username', $_SESSION['username']);
    $main->setContent('user', $user_greetings->get() );
    $prodotto->setContent('commenta', cercaPaginadaDescrizione('rating').",".$ris[0]['id'].$e_mex);
}
else{
    $side_login = new Template('skin/dtml/b_side_login.html');
    $side_login->setContent('id_page', $id_pagina."&id_prod=".$_GET['id_prod']);
    $main->setContent('login',$side_login->get());
    $prodotto->setContent('commenta', "");
}

###############################

##########
#impostazioni varie
$prodotto->setContent('image_large',$ris[0]['image_large']);
$prodotto->setContent('testo_alt',nl2br($ris[0]['alt_text']));
$prodotto->setContent('titolo_prod',nl2br($ris[0]['name']));
$prodotto->setContent('percorsoProd',$ris[0]);
$prodotto->setContent('avg_rating',$ris[0]['rating']);
$prodotto->setContent('tot_voti',"( ".$ris[0]['votes']." votes )");
$prodotto->setContent('prezzo',$ris[0]['price']);
$prodotto->setContent('quantita',$ris[0]['qty']);
$prodotto->setContent('nome2_prod',nl2br($ris[0]['name']));
$prodotto->setContent('lunga_descr',nl2br($ris[0]['long_descr']));
$prodotto->setContent('breve_descr',nl2br($ris[0]['brief_descr']));
$prodotto->setContent('option_qty',$ris[0]['qty']);
$prodotto->setContent('specs',$ris[0]['sensitivity']."|".$ris[0]['impedence']);
$prodotto->setContent('features',$ris[0]['features']);
//$prodotto->setContent('id_prod', $ris[0]['id']);
$prodotto->setContent('id_prod2', $ris[0]['id']);
$prodotto->setContent('id_page2', cercaPaginadaDescrizione('cart'));
$prodotto->setContent('prezzo2',$ris[0]['price']);

$query = "SELECT * FROM 5_rating WHERE id_prod = {$ris[0]['id']} ORDER BY data DESC LIMIT 0,10 ;";
$ris = getResult($query);
$prodotto->setContent('commenti',$ris);

##################################################
#sostituzione dei placehloder e close della pagina
$ris = menu('header');
$nav_bar->setContent('main_menu', $ris);
$search_bar->setContent('script', cercaPaginadaDescrizione('search'));
$nav_bar->setContent('search_bar', $search_bar->get());
$flt->setContent('cat', categorie());
$flt->setContent('rating', '');
$flt->setContent('priceRange', rangePrezzo());
$flt->setContent('discountRange', rangeSconto());
$main->setContent('inner_container', $prodotto->get());
$main->setContent('main_nav_bar',$nav_bar->get());
$main->setContent('side_filters', $flt->get());
$main->close();
##################################################
?>
