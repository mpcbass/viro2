<?php
session_start();

require_once 'include/dbms.inc.php';
require_once 'include/template2.inc.php';
require_once 'include/utilities.inc.php';

$id_pagina = $_GET['id'];
unset($_GET['id']);
if(isset($_GET['el'])){
    $da = $_GET['el'];
    unset($_GET['el']);
}
else{
    $da = 0;
}
if(isset($_GET['s'])){
    $search_parameter = $_GET['s'];
}
else{
    $search_parameter = "";
}

$main = new Template('skin/dtml/t_frame-public.html');
$nav_bar = new Template('skin/dtml/b_main_nav_bar.html');
$griglia = new Template('skin/dtml/b_product_grid.html');
$search_bar = new Template('skin/dtml/b_search_bar.html');
$flt = new Template('skin/dtml/b_side_filters3.html');

$search_bar->setContent('script', cercaPaginadaDescrizione('search'));
$nav_bar->setContent('search_bar', $search_bar->get());

$menu = menu('header');
$nav_bar->setContent('main_menu', $menu);

#########
#carrello
$carrello = carrello();
if($carrello != NULL){
    $top_cart = new Template('skin/dtml/b_cart.html');
    $top_cart->setContent('price_amount', number_format($carrello[0],2));
    $top_cart->setContent('item_num', $carrello[1]);
    $top_cart->setContent('item_plural', $carrello[2]);
    $top_cart->setContent('id_cart', cercaPaginadaDescrizione('cart'));
    $main->setContent('cart', $top_cart->get());
}else{$top_cart = new Template('skin/dtml/b_cart.html');
    $top_cart->setContent('price_amount', '0.00');
    $top_cart->setContent('item_num', '0');
    $top_cart->setContent('item_plural', "");
    $top_cart->setContent('id_cart', cercaPaginadaDescrizione('cart'));
    $main->setContent('cart', $top_cart->get());
}
#########

################################
#saluto utente oppure side login
if(isLogged()){
    $user_greetings = new Template('skin/dtml/b_side_user_greetings.html');
    $user_greetings->setContent('username', $_SESSION['username']);
    $main->setContent('user', $user_greetings->get() );

}
else{
    #$pagina = basename($_SERVER['PHP_SELF']);
    $side_login = new Template('skin/dtml/b_side_login.html');
    $side_login->setContent('id_page', cercaPaginadaDescrizione('search'));
    $main->setContent('login',$side_login->get());
}

################################

#################################################
#query e impostazioni per la griglia dei prodotti
/*$griglia = new Template('skin/dtml/b_product_grid_search.html');
$query = "SELECT * FROM 5_product WHERE name LIKE '%{$search_parameter}%'";
if($search_parameter == ""){
    $search_parameter = " ";
}
if(isset($_GET['m']) && isset($_GET['cat1'])){
    $query .= " AND brand = '{$_GET['m']}' AND id IN (SELECT id_product FROM 5_product_category WHERE id_category = {$_GET['cat1']}";
    if(isset($_GET['cat2'])){
        $query .= " OR id_category = {$_GET['cat2']} GROUP BY(id_product) HAVING COUNT(*) = 2 ";
        $info_page_selector['cat2'] = $_GET['cat2'];#param per setContent
    }
    $query .= ")";
    $info_page_selector['brand'] = $_GET['m'];#param per setContent
    $info_page_selector['cat1'] = $_GET['cat1'];
}
$query .= ";";
$res = getResult($query);
#parametri per setContent
$info_page_selector['num_righe'] = count($res);
$info_page_selector['da'] = $da;
$info_page_selector['range'] = 9;
$info_page_selector['search'] = $search_parameter;
$griglia->setContent('selettorePagine', $info_page_selector);
$elementi_in_pagina = array();
for($i = $da; $i < count($res);$i++){
    $elementi_in_pagina[] = $res[$i];
}
if($res == 0){
    $elementi_in_pagina = 0;
}
$griglia->setContent('prodotti', $elementi_in_pagina);
$griglia->setContent('titolo','results');
##################################################
*/

$flt->setContent('cat', categorie());
$flt->setContent('rating', '');
$flt->setContent('priceRange', rangePrezzo());
$flt->setContent('discountRange', rangeSconto());
$main->setContent('side_filters', $flt->get());
//$main->setContent('inner_container', $griglia->get());
$main->setContent('main_nav_bar', $nav_bar->get());
$scripts = array();
$scripts[] = "product_grid.js";
$main->setContent('scripts',$scripts);
$main->close();
?>
