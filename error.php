<?php

require_once 'include/utilities.inc.php';

    if(isset($_GET) && isset($_GET['e_type']))
    {
        if($_GET['e_type'] == "auth")
        {
            header("location:index.php?id=".cercaPaginadaDescrizione('login')."&err=do login for gain access to the requested page");
            #header('HTTP/1.1 403 Forbidden');
        }
        else if($_GET['e_type'] == "rating"){
            header('HTTP/1.1 400 Bad Request');
        }
        else if($_GET['e_type'] == "log_fail"){
            header("location:index.php?id=".cercaPaginadaDescrizione('login')."&err=invalid username or password");
        }
        #errore form registrazione
        else if(preg_match("/^add_user_fail/",$_GET['e_type'])){
            $stringa = preg_replace("/^add_user_fail /", "", $_GET['e_type']);
            unset($_GET['e_type']);
            foreach($_GET as $k => $v){
                $stringa .= "&{$k}={$v}";
            } 
            header("location:index.php?id=5&err=".$stringa);
        }
        else if(preg_match("/^add_prod_fail/",$_GET['e_type'])){
            $stringa = preg_replace("/^add_prod_fail /", "", $_GET['e_type']);
            unset($_GET['e_type']);
            foreach($_GET as $k => $v){
                $stringa .= "&{$k}={$v}";
            } 
            header("location:index.php?id=".cercaPaginadaDescrizione('manage_prod')."&err=".$stringa."&add_prod=");
        }
    }

?>
