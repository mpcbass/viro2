<?php

session_start();
require_once 'include/template2.inc.php';
require_once 'include/dbms.inc.php';
require_once 'include/utilities.inc.php';

$this_page = $_GET['id'];

if (isset($_GET['del'])) {
    $query = "DELETE FROM 1_user WHERE username = '{$_GET['username']}';";
    #RELAZIONI GESTITE DAL TRIGGER elimina_relazioni_utente
    queryInsert($query);
    header("location:index.php");
} else if (isset($_GET['add'])) {
    if ($_GET['password'] == $_GET['confirm_pwd']) {
        $pwd = cripta_password($_GET['password'], $_GET['username']);
        if(!preg_match('/[\w]{4}[\/\-]/',$_GET['date_of_birth'])){
            $_GET['date_of_birth'] = preg_replace("/([\w]+)([\/\-])([\w]+)([\/\-])([\w]+)/", "\$1,\$3,\$5", $_GET['date_of_birth']);
            $_GET['date_of_birth'] = "STR_TO_DATE('{$_GET['date_of_birth']}','%d,%m,%Y')";            
        }
        $query = "INSERT INTO 1_user VALUES('','{$_GET['username']}','{$pwd}','{$_GET['name']}','{$_GET['surname']}','{$_GET['date_of_birth']}','{$_GET['email']}','{$_GET['phone']}');";
        if (queryInsert($query)) {
            $id = getResult("SELECT id FROM 1_user WHERE username = '{$_GET['username']}';");
            $query = "INSERT INTO 1_user_group VALUES({$id[0]['id']},2);";
            queryInsert($query);
            header("location:index.php?mex=new user id: " . $id[0]['id']);
        } else {
            header("location:index.php?id=" . $this_page . "&err=parameters not valid!");
        }
    } else {
        header("location:index.php?id=".$this_page."&err=Password do not correspond!");#da completare
    }
} else {
    $main = new Template('skin/dtml/t_frame_private.html');
    $nav_bar = new Template('skin/dtml/b_main_nav_bar.html');
    $side_menu = new Template('skin/dtml/b_side_menus.html');
    $form = new Template('skin/dtml/b_manage_user.html');
    $query = "SELECT * FROM 1_group WHERE descr <> 't_user';";
    $ris = getResult($query);
    $form->setContent('group_list', $ris);
    $form->setContent('id_page', $this_page);
    $form->setContent('id_page1', $this_page);

    if (isset($_GET['err'])) {
        $form->setContent('error_message', "<p>" . $GET['err'] . "</p>");
    }

    $ris = menu('header_admin');
    $nav_bar->setContent('main_menu', $ris);
    $side_menu->setContent('page_cat', cercaPaginadaDescrizione('add_cat'));
    $side_menu->setContent('page_slide', cercaPaginadaDescrizione('slides'));
    $side_menu->setContent('page_order', cercaPaginadaDescrizione('manage_order'));
    $side_menu->setContent('page_user', $this_page);
    $side_menu->setContent('page_muser', cercaPaginadaDescrizione('modify_user'));
    $side_menu->setContent('page_mprod', cercaPaginadaDescrizione('modify_product'));
    $side_menu->setContent('page_adprod', cercaPaginadaDescrizione('manage_prod'));
    $main->setContent('inner_container', $form->get());
    $main->setContent('side_menu', $side_menu->get());
    $main->setContent('main_nav_bar', $nav_bar->get());
    $main->close();
}
?>
