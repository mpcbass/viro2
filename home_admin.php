<?php
session_start();

require_once 'include/utilities.inc.php';
require_once 'include/template2.inc.php';

$main = new Template('skin/dtml/t_frame_private.html');
$nav_bar = new Template('skin/dtml/b_main_nav_bar.html');
$side_menu = new Template('skin/dtml/b_side_menus.html');

$ris = menu('header_admin');
$nav_bar->setContent('main_menu', $ris);
if(isset($_GET['mex'])){
    $main->setContent('mex',$_GET['mex']);
    $scripts=array();
    $scripts[]='adminTimedMsg.js';
    $main->setContent('scripts',$scripts);
}
$page_cat = cercaPaginadaDescrizione('add_cat');
$side_menu->setContent('page_cat', $page_cat);
$side_menu->setContent('page_slide', cercaPaginadaDescrizione('slides'));
$side_menu->setContent('page_order', cercaPaginadaDescrizione('manage_order'));
$side_menu->setContent('page_adprod', cercaPaginadaDescrizione('manage_prod'));
$side_menu->setContent('page_user', cercaPaginadaDescrizione('manage_user'));
$side_menu->setContent('page_muser', cercaPaginadaDescrizione('modify_user'));
$side_menu->setContent('page_mprod', cercaPaginadaDescrizione('modify_product'));
$main->setContent('side_menu', $side_menu->get());
$main->setContent('main_nav_bar',$nav_bar->get());
$main->close();

?>
